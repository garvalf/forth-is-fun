\ garvalf mode hercules

include hercule

: garvalf  gmode g-dark page1 
243 220 310 153  draw page1 
310 153 273 89  draw page1 
273 89 261 14  draw page1 
261 14 200 43  draw page1 
200 43 162 2  draw page1 
162 2 153 42  draw page1 
153 42 231 58  draw page1 
231 58 153 42  draw page1 
153 42 130 46  draw page1 
130 46 123 52  draw page1 
123 52 179 59  draw page1 
179 59 231 58  draw page1 
123 52 117 66  draw page1 
117 66 158 75  draw page1 
117 66 68 70  draw page1 
68 70 158 75  draw page1 
68 70 58 100  draw page1 
58 100 140 151  draw page1 
58 100 105 141  draw page1 
105 141 68 145  draw page1 
105 141 140 151  draw page1 
68 145 140 151  draw page1 
68 145 63 152  draw page1 
63 152 75 169  draw page1 
75 169 145 173  draw page1 
145 173 167 202  draw page1 
167 202 243 220  draw page1 
145 173 197 178  draw page1 
197 178 243 220  draw  
;




: eli 
gmode g-dark page1 360 174 145 45 10 200 500 ellipse
;


: run garvalf ;
