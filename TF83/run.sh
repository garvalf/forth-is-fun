
help() {
	printf "\nStart Turbo Forth 83 "
	printf "\n---------------------- \n"
	printf "command line arguments: \n"
	printf "./run.sh hercules : run monochrome hercules graphics on 8086 then type tf086 and load 'include garvalfh' \n"
	printf "./run.sh cga : run cga graphics (on 386) then type tf386 and load 'include garvalf' \n\n"
}


hercules() {
	dosbox-x -conf dosbox-x-hercules.conf ./ 
}

cga() {
	dosbox-x  -machine=cga ./ 
}




if [ $# -eq 0 ]
then 
	help
fi


if [ $# -eq 1 ]
	then 
		if [ $1 == 'hercules' ]
			then
				hercules
	 else
		if [ $1 == 'cga' ]
		then
				cga
			else 
			printf "\n type 'cga' or 'hercules' as argument \n"
		fi
		fi
fi