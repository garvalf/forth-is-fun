\ --------------------------------------------
\                                           :;
\         Octagram patterns                 :;
\                                           :; 
\ Turtle logo graphics for waforth/thurtle  :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  

\ for using with https://mko.re/waforth/thurtle/
\ 2024 

\ https://en.wikipedia.org/wiki/Octagram

\ French translation of default words:

: avance FORWARD ;      \ ( n -- ): Move forward by n.
: recule BACKWARD ;     \ ( n -- ): Move backward by n.
: gauche LEFT ;         \ ( n -- ): Turn left by n degrees.
: droite RIGHT ;        \ ( n -- ): Turn right by n degrees.
: aller SETXY ;         \ ( n1 n2 -- ): Move to position n1,n2.
: oriente SETHEADING ;  \ ( n -- ): Set heading n degrees clockwise from Y axis.
: lève    PENUP ;       \ ( -- ): Disable drawing while moving.
: pose    PENDOWN ;     \ ( -- ): Enable drawing while moving.
: crayon  ;
: taille SETPENSIZE ;   \ ( n -- ): Set the width of the drawed strokes to n (default: 5).
: couleur SETPENCOLOR ; \ ( n -- ): Set the color of the drawed strokes to RGB value n (default: 0).
: fond SETSCREENCOLOR ; \ ( n -- ): Set the color of the screen to RGB value n. (default: 0).
: écran ;
: cache HIDETURTLE ;    \ ( -- ): Hide the turtle.
: montre SHOWTURTLE ;   \ ( -- ): Show the turtle.
: tortue  ;

: si POSTPONE IF ; IMMEDIATE
: ensuite POSTPONE THEN ; IMMEDIATE
: sinon POSTPONE ELSE ; IMMEDIATE

: compte POSTPONE DO ; IMMEDIATE
: boucle POSTPONE LOOP ; IMMEDIATE
: +boucle POSTPONE +LOOP ; IMMEDIATE


\ being more lazy

: av avance ;
: ga gauche ;
: dr droite ; 

: fw FORWARD ;
: lf LEFT ;
: rt RIGHT ;

VARIABLE n1
VARIABLE n2
VARIABLE size
VARIABLE density
VARIABLE décalagex
VARIABLE décalagey
VARIABLE nb
VARIABLE décalage

cache tortue


: pointe  ( x y -- )   \ absolute plot
lève 
 aller 
pose
;



: déplace ( x y -- )   \ relative plot
SWAP
90 oriente 
lève
 avance
0 oriente
 avance
pose
;

: mozaique ( size -- )  \ star_compound
n1 !
270 oriente
8 0 DO
n1 @ avance
45 gauche
n1 @ avance
90 droite
LOOP
;

: mozaique_in ( size -- )  \ star_compound
n1 !
270 oriente
4 0 DO
2 0 DO
  n1 @ avance
  45 droite
  n1 @ avance
  90 gauche
LOOP
  180 droite
LOOP


;

: mozaique2 ( size offset -- )  \ for double 
décalage !
n1 !
270 oriente

 décalage @ 2 * décalage @ 10 / 2 * - DUP 
    décalage @ 0 < IF  ELSE NEGATE THEN déplace
\ 36 36 NEGATE déplace

8 0 DO
n1 @ décalage @ + avance 
45 gauche
n1 @ décalage @ + avance
90 droite
LOOP
;


\ 

: star_regular ( size -- )
n1 !
270 oriente
8 0 DO
n1 @ avance
90 gauche
n1 @ avance
135 droite
LOOP
;

: form01 ( size -- )  \ spirograph...
n1 !
270 oriente
24 0 DO
n1 @ avance
25 gauche
n1 @ avance
95 droite
LOOP
;



: enneagram ( size -- ) \ 9 angles star
n1 !
270 oriente
10 0 DO
n1 @ avance
65 gauche
n1 @ avance
145 droite
LOOP
;


: pavage ( nb size density décalagex décalagey -- )
décalagey !
décalagex !
density !
size !
nb !
nb @ 0 DO
  nb @ 0 DO
  size @ 4 J * * décalagex @ + size @ 4 I  * * décalagey @ + pointe
  size @ mozaique
\  size @ 0 mozaique2
\  size @ 10 mozaique2
  density @  +LOOP
density @  +LOOP
;


: pavage2 ( nb size density décalagex décalagey -- )
décalagey !
décalagex !
density !
size !
nb !
nb @ 0 DO
  nb @ 0 DO
  size @ 4 J * * décalagex @ + size @ 4 I  * * décalagey @ + pointe
\  size @ mozaique2
  size @ 0 mozaique2
  size @ 10 mozaique2
  density @  +LOOP
density @  +LOOP
;

: motif ( nb size density -- )
density !
size !
nb !
nb @ 0 DO
  nb @ 0 DO
  size @  J * size @  I  * déplace
  size @ mozaique
  density @  +LOOP
density @  +LOOP

;


: run 
11 100 1 0 0 pavage
15 taille
5 200 2 230 230 pavage
;


HEX FB8C00 DECIMAL CONSTANT orange

: run2

40 taille
\ 1485 1150 pointe
1379 1040 pointe
16777215 couleur
462 mozaique_in
462 mozaique

4 taille
2000 couleur
11 100 1 0 0 pavage2



\ use a constant to define hexadecimal colors
\ or convert it elsewhere to decimal
\ in gforth for example:
\ $FB8C00 10 base ! u. 
\ HEX 299399 fond DECIMAL

\ https://htmlcolorcodes.com/color-picker/

orange fond
16485376 fond écran

15 taille
43500 couleur
5 200 2 230  -110 pavage


;

\ run
 run2
\ 100 enneagram

