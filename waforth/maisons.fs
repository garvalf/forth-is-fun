\ --------------------------------------------
\                                           :;
\           Drawing houses                  :;
\                                           :; 
\ Turtle logo graphics for waforth/thurtle  :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  

\ for using with https://mko.re/waforth/thurtle/
\ 2024 

\ French translation of default words:


: avance FORWARD ;      \ ( n -- ): Move forward by n.
: recule BACKWARD ;     \ ( n -- ): Move backward by n.
: gauche LEFT ;         \ ( n -- ): Turn left by n degrees.
: droite RIGHT ;        \ ( n -- ): Turn right by n degrees.
: aller SETXY ;         \ ( n1 n2 -- ): Move to position n1,n2.
: oriente SETHEADING ;  \ ( n -- ): Set heading n degrees clockwise from Y axis.
: lève    PENUP ;       \ ( -- ): Disable drawing while moving.
: pose    PENDOWN ;     \ ( -- ): Enable drawing while moving.
: crayon  ;
: taille SETPENSIZE ;   \ ( n -- ): Set the width of the drawed strokes to n (default: 5).
: couleur SETPENCOLOR ; \ ( n -- ): Set the color of the drawed strokes to RGB value n (default: 0).
: fond SETSCREENCOLOR ; \ ( n -- ): Set the color of the screen to RGB value n. (default: 0).
: écran ;
: cache HIDETURTLE ;    \ ( -- ): Hide the turtle.
: montre SHOWTURTLE ;   \ ( -- ): Show the turtle.
: tortue  ;
: av avance ;
: ga gauche ;
: dr droite ; 

VARIABLE RND
HERE RND !

: RANDOM0 ( -- n )
  RND @ 75 * 74 + 65537 MOD
  DUP RND !
;

: rnd6 RANDOM 6 MOD ;


HEX 99CCFF DECIMAL CONSTANT bleu

bleu fond

: maison
270 oriente  \ 90° à droite
90 gauche
100 avance
45 gauche
100 avance
90 ga 100 av
45 ga 100 av
90 ga 350 av
;


: maisons
rnd6 rnd6 + 1 + 0 DO maison LOOP ;




: 1/4-cercle ( taille -- )
4 0 DO DUP
( taille ) av 20 ga DUP ( taille ) av 
LOOP DROP 
;

: demi-cercle ( taille -- )
9 0 DO DUP
( taille ) av 20 ga DUP ( taille ) av 
LOOP DROP 
;

: 1/2-cercle demi-cercle ;
: 2/4-cercle demi-cercle ;

: 3/4-cercle ( taille -- )
11 0 DO DUP
( taille ) av 20 ga DUP ( taille ) av 
LOOP DROP 
;

: nuage0
0 oriente
\ cache tortue
20 demi-cercle 
5 demi-cercle
3 0 DO 
180 oriente
5 demi-cercle
LOOP
;

: nuage 
160 180 + oriente
20 3/4-cercle
10 demi-cercle
5 demi-cercle
lève 55 av pose
\ 45 oriente 
 65 droite
15 3/4-cercle
\ 90 oriente
190 droite
17 3/4-cercle
\ 90 30 + oriente
 190 gauche
18 3/4-cercle

\ 280 oriente
25 droite
\ 30 1/4-cercle
\ 55 droite
\ 20 1/4-cercle
31 1/4-cercle
160 droite
10 2/4-cercle
;

: paysage
lève crayon 180 oriente 1300 avance pose crayon
maisons 
;


nuage
lève crayon
270 oriente 800 av 
pose 
\ nuage
paysage

