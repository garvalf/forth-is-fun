\ --------------------------------------------
\                                           :;
\         Pyramid  patterns                 :;
\                                           :; 
\ Turtle logo graphics for waforth/thurtle  :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  

\ for using with https://mko.re/waforth/thurtle/
\ 2024 

\ https://mko.re/waforth/thurtle/?pn=Pyramid&ar=1&u=AlwgLacAAgpcIKYAATo7hy4QUHlyYW1pZCAgcGF0dGVybnOaLqBdJCAKXCBUdXJ0bGUgbG9nbyBncmFwaGljcyBmb3IgRFgtRk9SVEi2XikKXCBodHRwczovL2dpdGxhYi5jb20vZ2FydmFsZi9mb3J0aC1pcy1mdW60vAAtpwCAvAIKClyB2wl1c2luZyB3aXRohaEIbWtvLnJlL3dhgZckL3RodXJ0bGUvClwgMjAyNCAKCjogYXZhbmNlIEZPUldBUkQgO4L_D1wgKCBuIC0tICk6IE1vdmWAYBd3YXJkIGJ5IG4uCjogcmVjdWxlIEJBQ0uHN402A2JhY2uJNwpnYXVjaGUgTEVGVIRri24IVHVybiBsZWZ0gWsHIGRlZ3JlZXOAcwpkcm9pdGUgUklHSIc8jTsDcmlnaI48CmFsbGVyIFNFVFhZjHgDMSBuMojrEHRvIHBvc2l0aW9uIG4xLG4ygPAGb3JpZW50ZYBBBkhFQURJTkeA84jwClNldCBoZWFkaW5nhrcUIGNsb2Nrd2lzZSBmcm9tIFkgYXhpgc0EbMOodmWA_wRQRU5VUIXPgM6CzAtEaXNhYmxlIGRyYXeAUwt3aGlsZSBtb3ZpbmeA0QJwb3OEPwNET1dOg9WGPwFFbpk-EGNyYXlvbiAgOwo6IHRhaWxsgdsGUEVOU0laRYGOjNwLdGhlIHdpZHRoIG9mgQyAmRtlZCBzdHJva2VzIHRvIG4gKGRlZmF1bHQ6IDUpgWYFb3VsZXVyg1sGQ09MT1IgO5FaBGNvbG9ylloIUkdCIHZhbHVliWQAMIFkA2ZvbmSAvQNTQ1JFp2QFc2NyZWVui1wALo1dB8OpY3JhbiA7gM0TYWNoZSBISURFVFVSVExFIDsgICCBzYLLA0hpZGWBzAV0dXJ0bGWAmwptb250cmUgU0hPV4czhjIDU2hvd4syMHRvcnR1ZSAgOwoKOiBzaSBQT1NUUE9ORSBJRiA7IElNTUVESUFURQo6IGVuc3VpdGWGIQNUSEVOiyMEc2lub26GQwJFTFOAtoZFgNYCb21wiEUBRE-LZwRib3VjbIdmA0xPT1CLigArjCMAK40kCwo6IGR1cCBEVVAgO4ALBXJvcCBEUoBJgegQcGlyYWxlICggbiAtLSApCiCBKQc4MDAgPiBzaYIpA0VYSVSF9YQhASAvgVcJYXZhbmNlIDMgMITQgG2AcQg0NSBnYXVjaGWEIwE0NYB9gEODNIPggJIFCiAgMTIwgx88CiAgOCArCiAgUkVDVVJTRQo7CgoxNjc2Nzc5NSBmb25kCjQ1MzcxMDEgY291bGV1cgoKMiB0YWlsbGUKMYTSAAo


: avance FORWARD ;      \ ( n -- ): Move forward by n.
: recule BACKWARD ;     \ ( n -- ): Move backward by n.
: gauche LEFT ;         \ ( n -- ): Turn left by n degrees.
: droite RIGHT ;        \ ( n -- ): Turn right by n degrees.
: aller SETXY ;         \ ( n1 n2 -- ): Move to position n1,n2.
: oriente SETHEADING ;  \ ( n -- ): Set heading n degrees clockwise from Y axis.
: lève    PENUP ;       \ ( -- ): Disable drawing while moving.
: pose    PENDOWN ;     \ ( -- ): Enable drawing while moving.
: crayon  ;
: taille SETPENSIZE ;   \ ( n -- ): Set the width of the drawed strokes to n (default: 5).
: couleur SETPENCOLOR ; \ ( n -- ): Set the color of the drawed strokes to RGB value n (default: 0).
: fond SETSCREENCOLOR ; \ ( n -- ): Set the color of the screen to RGB value n. (default: 0).
: écran ;
: cache HIDETURTLE ;    \ ( -- ): Hide the turtle.
: montre SHOWTURTLE ;   \ ( -- ): Show the turtle.
: tortue  ;

: si POSTPONE IF ; IMMEDIATE
: ensuite POSTPONE THEN ; IMMEDIATE
: sinon POSTPONE ELSE ; IMMEDIATE

: compte POSTPONE DO ; IMMEDIATE
: boucle POSTPONE LOOP ; IMMEDIATE
: +boucle POSTPONE +LOOP ; IMMEDIATE

: dup DUP ;
: drop DROP ;

: spirale ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 8 / dup avance 3 0 compte dup dup 45 gauche avance 45 droite avance boucle drop
  120 droite
  8 +
  RECURSE
;


: spirale2 ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 8 / dup avance 3 0 compte dup dup 
60 gauche avance 60 droite avance boucle drop
  121 droite
  8 +
  RECURSE
;

16767795 fond
4537101 couleur

2 taille
1 spirale
