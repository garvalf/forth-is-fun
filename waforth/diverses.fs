\ --------------------------------------------
\                                           :;
\           Some sketches for               :;
\                                           :; 
\            waforth/thurtle                :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  

\ for using with https://mko.re/waforth/thurtle/
\ 2024 


: avance FORWARD ;      \ ( n -- ): Move forward by n.
: recule BACKWARD ;     \ ( n -- ): Move backward by n.
: gauche LEFT ;         \ ( n -- ): Turn left by n degrees.
: droite RIGHT ;        \ ( n -- ): Turn right by n degrees.
: aller SETXY ;         \ ( n1 n2 -- ): Move to position n1,n2.
: oriente SETHEADING ;  \ ( n -- ): Set heading n degrees clockwise from Y axis.
: lève    PENUP ;       \ ( -- ): Disable drawing while moving.
: pose    PENDOWN ;     \ ( -- ): Enable drawing while moving.
: crayon  ;
: taille SETPENSIZE ;   \ ( n -- ): Set the width of the drawed strokes to n (default: 5).
: couleur SETPENCOLOR ; \ ( n -- ): Set the color of the drawed strokes to RGB value n (default: 0).
: fond SETSCREENCOLOR ; \ ( n -- ): Set the color of the screen to RGB value n. (default: 0).
: écran ;
: cache HIDETURTLE ;    \ ( -- ): Hide the turtle.
: montre SHOWTURTLE ;   \ ( -- ): Show the turtle.
: tortue  ;

: si POSTPONE IF ; IMMEDIATE
: ensuite POSTPONE THEN ; IMMEDIATE
: sinon POSTPONE ELSE ; IMMEDIATE

: compte POSTPONE DO ; IMMEDIATE
: boucle POSTPONE LOOP ; IMMEDIATE
: +boucle POSTPONE +LOOP ; IMMEDIATE

: dup DUP ;
: drop DROP ;

: spirale ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 8 / dup avance 3 0 compte dup dup 
45 gauche avance 45 droite avance boucle drop
  120 droite
  8 +
  RECURSE
;

: spirale2 ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 8 / dup avance 3 0 compte dup dup 
90 gauche avance 45 droite avance boucle drop
  120 droite
  8 +
  RECURSE
;

: spirale3 ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 8 / dup avance 3 0 compte dup dup 
94 gauche I 2 * * avance 45 droite avance boucle drop
  120 droite
  8 +
  RECURSE
;


: spirale4 ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 8 / dup avance 3 0 compte dup dup 
94 gauche I 2 * * avance 90 droite avance boucle drop
  120 droite
  8 +
  RECURSE
;

: spirale5 ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 8 / dup avance 3 0 compte dup dup 
90 gauche I 2 * * avance 90 droite avance 
boucle drop
  90 droite
  8 +
  RECURSE
;



: spirale6 ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 11 / dup avance 8 0 compte dup dup 
95 gauche I 3 * * avance 95 droite avance 
20 gauche 10 avance
 
boucle drop
  90 droite
  15 +
  RECURSE
;


: spirale7 ( n -- )
  DUP 800 > si DROP EXIT ensuite 
  DUP 11 / dup avance 28 0 compte dup dup 
95 gauche I 3 * * avance 91 droite avance 
20 gauche I 10 * I * avance
 
boucle drop
  90 droite
  15 +
  RECURSE
;

: spirale8 ( n -- )
  DUP 400 > si DROP EXIT ensuite 
  DUP 11 / dup avance 28 0 compte dup dup 
95 gauche I 3 * * avance 86 droite avance 
20 gauche I 10 * I * avance
 
boucle drop
  90 droite
  15 +
  RECURSE
;


: spirale9 ( n -- )
  DUP 300 > si DROP EXIT ensuite 
  DUP 18 / dup avance 28 0 compte dup dup 
95 gauche I 3 * * avance 86 droite avance 
19 gauche I 11 * I * avance
 
boucle drop
  90 droite
  15 +
  RECURSE
;

cache

16767795 fond
4537101 couleur
247 oriente
18 taille
40 spirale2

