\ --------------------------------------------
\                                           :;
\           Golden ratio fun                :;
\                                           :; 
\ Turtle logo graphics for waforth/thurtle  :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  

\ for using with https://mko.re/waforth/thurtle/
\ 2024 


HEX FFDD55 DECIMAL CONSTANT yellow
yellow SETSCREENCOLOR


: golden ( n -- n golden )
 DUP 1618 * 1000 / SWAP
;

: goldenspiral ( nb length -- )
SWAP 0 DO golden SWAP DUP ROT ROT SWAP FORWARD 60 LEFT FORWARD 90 LEFT LOOP
;

: goldenrspiral ( nb length -- )
SWAP 0 DO golden SWAP DUP ROT ROT SWAP FORWARD 60 RIGHT FORWARD 90 LEFT LOOP
;

: goldenbox ( length -- )
DUP golden FORWARD 90 LEFT FORWARD 90 LEFT 
    golden FORWARD 90 LEFT FORWARD
;

HIDETURTLE

: demo01
50 SETPENSIZE 
13 10 goldenspiral
6 105 goldenrspiral
;

: demo02
3 SETPENSIZE
15 0 DO
105 goldenbox
35 LEFT
56 goldenbox
LOOP
;

: demo03
3 SETPENSIZE
15 0 DO
105 I 6 * + goldenbox
35 LEFT
56 I 3 * + goldenbox
LOOP
;


: demo04
3 SETPENSIZE
40 0 DO
105 I 6 * + goldenbox
20 LEFT
LOOP
;


: demo05
6 SETPENSIZE
40 0 DO
105 I 6 * + goldenbox
20 LEFT
LOOP
60 0 DO
800 I 2 * + goldenbox
75 LEFT
LOOP
;

: demo06
8 SETPENSIZE
45 0 DO
105 I 6 * + goldenbox
45 LEFT
LOOP
45 0 DO
800 I 2 * + goldenbox
75 LEFT
LOOP
;

: demo07
8 SETPENSIZE
105 goldenbox
100 FORWARD
80 goldenbox
100 FORWARD
30 goldenbox
100 FORWARD
40 goldenbox
100 FORWARD
;

: demo08
4 SETPENSIZE
4 0 DO 
RANDOM 100 MOD goldenbox
100 FORWARD
LOOP
;

: goldenbox02 ( length -- )
DUP golden FORWARD 60 LEFT FORWARD 60 LEFT 
    golden FORWARD 60 RIGHT FORWARD
;

: demo09
4 SETPENSIZE
8 0 DO 
100 goldenbox02
100 FORWARD
LOOP
;


: goldenbox04 ( length -- )
DUP DUP golden FORWARD 60 LEFT FORWARD 60 LEFT 
    golden FORWARD 60 RIGHT FORWARD
    golden FORWARD 55 RIGHT FORWARD
;

: goldenbox05 ( length -- )
DUP DUP golden FORWARD 60 RIGHT FORWARD 60 RIGHT 
    golden FORWARD 90 LEFT FORWARD
    golden FORWARD 90 LEFT FORWARD
;


: demo10
6 SETPENSIZE
128 0 DO 
100 goldenbox04
100 FORWARD
20 goldenbox05
20 FORWARD
LOOP
;

: demo11
6 SETPENSIZE
128 0 DO 
100 goldenbox
100 FORWARD
20 goldenbox05
20 FORWARD
LOOP
;


: demo12
8 0 DO
demo11
PENUP
400 FORWARD
45 RIGHT
PENDOWN
LOOP
;

demo04