\ WIP

also forth internals

variable myVar
variable myVarLen

variable w      \ width
variable t      \ thickness

: testInput
." tapez un nombre (4 chiffres max):"
PAD 4 ACCEPT myVarLen !
PAD myVarLen @ s>number? drop myVar ! 
." Vous avez tapé : " myVar @ .
;


: input
PAD 4 ACCEPT myVarLen !
PAD myVarLen @ s>number? drop myVar ! 
;

: gr ;

: lineWidth drop ;
: color! drop ;
: fillText drop ;
: font drop ;

: strokeText { a n x y -- }  ;
: bold { } ;
: textAlign { a n -- } ;  
: setLineDash { x y } ;
: setLineDash4 { w x y z } ;

: testLanguage { -- x }
1  \ French
\ 2 \ English
 ;

: english
;

: français
;

: askWidth { -- x } 
 testLanguage 1 = IF
  ." Cette page va générer un patron d'origami pour réaliser des pochettes en V pour vos jeux de cartes." CR
  ." Quelle est la largeur des cartes (en mm) ?"
  ELSE
  ." This page will generate an origami pattern to make V-shaped pockets for your decks of cards." CR
  ." How wide are the cards (in mm)?"
  THEN
  input
  myVar @ \ w !
;

: askThickness { -- x } 
 testLanguage 1 = IF 
 ." Quelle est l'épaisseur du paquet (en mm) ?" 
  ELSE
 ." How thick is the deck (in mm)?"
 THEN 
 input myVar @ \ t !
;


include bresenham.fs
include origami.fs




