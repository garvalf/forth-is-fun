web definitions 
 
JSWORD: strokeText { a n x y -- }  
  context.ctx.strokeText(GetString(a, n), x, y);  
~  
JSWORD: bold { } 
    var bold = document.createElement('b');
    context.terminal.append(bold);

~ 
\ possible values: start end left right or center; default: start 
JSWORD: textAlign { a n -- }  
  context.ctx.textAlign = GetString(a, n);  
~  

JSWORD: setLineDash { x y }
  context.ctx.setLineDash([x,y]);
~

JSWORD: setLineDash4 { w x y z }
  context.ctx.setLineDash([w,x,y,z]);
  
~
JSWORD: testme { -- s }
  var Width = window.prompt("Write a number");
  let WidthFloat = Width + 'e';
  let trucmuche = GetString(WidthFloat,10);
  return trucmuche;
~
JSWORD: askWidth { -- x }
 if (navigator.language == 'fr') {
  var Width = window.prompt("Cette page va générer un patron d'origami pour réaliser des pochettes en V pour vos jeux de cartes. \n\n Quelle est la largeur des cartes (en mm) ?")
  } else
  {
    var Width = window.prompt("This page will generate an origami pattern to make V-shaped pockets for your decks of cards. \n\n How wide are the cards (in mm)?")
  }
  //let truc2 = 'e';
  //let WidthFloat = Width + truc2;
  let WidthFloat = Width * 100;
  return WidthFloat;
~
JSWORD: askThickness { -- x }
 if (navigator.language == 'fr') {
  var Thickness = window.prompt("Quelle est l'épaisseur du paquet (en mm) ?")
  } else
  {
  var Thickness = window.prompt("How thick is the deck (in mm)?")
  }
  return Thickness
~
JSWORD: testLanguage { -- x }
 if (navigator.language == 'fr') {
  var Language = '1'
  } else
  {
  var Language = '2'
  }
  return Language
~

forth definitions

only web 
also forth internals