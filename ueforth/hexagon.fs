#! /usr/bin/env ueforth 

\ Bresenham's line algorithm & hexagons
\ 2023 Forth is Fun
\ https://gitlab.com/garvalf/forth-is-fun/
\ 
\ ueforth and additional code by:
\ Copyright 2022 Bradley D. Nelson
\ https://eforth.appspot.com/
\
\ Licensed under the Apache License, Version 2.0 (the "License");
\ you may not use this file except in compliance with the License.
\ You may obtain a copy of the License at
\
\     http://www.apache.org/licenses/LICENSE-2.0
\
\ Unless required by applicable law or agreed to in writing, software
\ distributed under the License is distributed on an "AS IS" BASIS,
\ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
\ See the License for the specific language governing permissions and
\ limitations under the License.

DEFINED? windows [IF] windows dpi-aware [THEN]
also graphics
also structures
also internals

struct ScanSpan
  ptr field ->next
  ptr field ->edge

2048 constant max-scanlines
create scanlines max-scanlines cells allot
scanlines max-scanlines cells erase
0 value free-edges


-1 -1 window

$ff0000 constant red
$ffff00 constant yellow
$ffffff constant white
$000000 constant black
$0000bb constant blue



: pix ( a b -- )
   pixel 1 red fill32
;

: pixb0 ( a b -- )
   pixel 1 black fill32
;

: pixb1 ( a b -- )
   pixel 1 white fill32
;

: pixb2 ( a b -- )
   pixel 1 red fill32
;

: pixb3 ( a b -- )
   pixel 1 white fill32
;




variable rand 
here rand !

: random rand @ 31421 * 6927 + dup rand ! ;

: rnd100 ( u1 -- u2 ) random random - random * abs 100 /mod + random + random * here + abs 100 /mod - abs 100 mod ;
: rnd40 ( u1 -- u2 ) random random - random * abs 40 /mod + random + random * here + abs 40 /mod - abs 40 mod ;
: rnd10 ( u1 -- u2 ) random random - random * abs 10 /mod + random + random * here + abs 10 /mod - abs 10 mod ;

: randomcolor  random random - random * abs 10 /mod + random + random * here + abs 16777215 /mod - abs 16777215 mod  ;

 
variable hexx0
variable hexy0
variable p1x  \ top of hexagon x
variable p1y  \ top of hexagon y
variable p2x  \ bottom of hexagon x
variable p2y  \ bottom of hexagon y


\ Bresenham's algorithm
\ \\\\\\\\\\\\\\\\\\\\\

variable x
variable y
variable x0
variable y0
variable x1
variable y1
variable deltax
variable deltay
variable delta
variable yi
variable xi

variable h
variable w




variable mycolor
1 mycolor !

: lineLow ( x0 y0 x1 y1 )
\ storing data from the stack
\  cover slopes between 0 and -1 by checking 
\ whether y needs to increase or decrease (i.e. dy < 0) 
y1 ! x1 ! y0 ! x0 !  
x1 @ x0 @ - deltax !
y1 @ y0 @ - deltay !
1 yi !

deltay @ 0 <= if
-1 yi !
\ we add one to invert otherwise five invert will be minus 6
deltay @ invert 1 + deltay !  
then

deltay @ 2 * deltax @ - delta !
y0 @ y !
x0 @ x !

x1 @ x0 @ do                    \  ( loop )
\ ." x= " x ? ." y= " y ? ." delta= " delta ? cr  ( debug ) 

mycolor @ 1 = if
 x @ y @ pixb1 else
mycolor @ 2 = if
 x @ y @ pixb2 else
mycolor @ 3 = if
 x @ y @ pixb3 else
then then then

delta @ 0 >= if
 y @ yi @ + y !
 delta @ deltay @ deltax @ - 2 * + delta !
 else
 delta @ deltay @ 2 * + delta !
then
 x @ 1 + x !
loop 
;


: lineHigh ( x0 y0 x1 y1 )
\ switching the x and y axis, 
\ for positive or negative steep slopes 
y1 ! x1 ! y0 ! x0 !  
x1 @ x0 @ - deltax !
y1 @ y0 @ - deltay !
1 xi !

deltax @ 0 <= if
-1 xi !
\ we add one to invert otherwise five invert will be minus 6
deltax @ invert 1 + deltax !  
then

deltax @ 2 * deltay @ - delta !
y0 @ y !
x0 @ x !

y1 @ y0 @ do                    \  ( for loop )
\ ." x= " x ? ." y= " y ? ." delta= " delta ? cr  ( debug ) 

mycolor @ 1 = if
 x @ y @ pixb1 else
mycolor @ 2 = if
 x @ y @ pixb2 else
mycolor @ 3 = if
 x @ y @ pixb3 else
then then then



delta @ 0 >= if
 x @ xi @ + x !
 delta @ deltax @ deltay @ - 2 * + delta !
 else
 delta @ deltax @ 2 * + delta !
then
 y @ 1 + y !
loop 
;

: line  ( x0 y0 x1 y1 )
y1 ! x1 ! y0 ! x0 ! 
y1 @ y0 @ - abs 
x1 @ x0 @ - abs
<= if 
 x0 @ x1 @ >= if
   x1 @ y1 @ x0 @ y0 @ lineLow
 else 
   x0 @ y0 @ x1 @ y1 @ lineLow
 then
else
 y0 @ y1 @ >= if
  x1 @ y1 @ x0 @ y0 @ lineHigh
 then 
  x0 @ y0 @ x1 @ y1 @ lineHigh
then 
;


\ end of Bresenham's algorithm
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\




: diamond

25 50 50 25 line
50 25 75 50 line 
75 50 50 75 line  
50 75 25 50 line  
;


: rect ( x0 y0 w h ) 
hexx0 @ x !
hexy0 @ y !
w @ -1 do     \ loop begin
  x @ hexy0 @ pix
  x @ hexy0 @ h @ + pix
  x @ 1 + x !
loop 
h @ -1 do    \ loop begin
  hexx0 @ y @ pix
  hexx0 @ w @ + y @ pix
  y @ 1 + y !
loop
;


: hexa
h ! w ! hexy0 ! hexx0 !
2 mycolor !
  \ rect
  w @ 2 /  h @ 2 / 
  hexy0 @ swap - 
  swap
  hexx0 @ +  swap p1y ! p1x ! \ pix
  
  w @ 2 /  h @ 2 / 
  hexy0 @ h @ +  + 
  swap
  hexx0 @ +  swap p2y ! p2x !  \ pix

hexx0 @ hexy0 @ p1x @ p1y @ line
hexx0 @ w @ + hexy0 @  p1x @ p1y @ line

 \ x0 @ y0 @ h @ + p2x @ p2y @ line
 
 hexx0 @ hexy0 @ h @ + p2x @ p2y @ line  
 hexx0 @ w @ + hexy0 @ h @ + p2x @ p2y @ line  
 \ hexx0 @ hexy0 @ p1x @ p1y @ line
 
 hexx0 @ hexy0 @  hexx0 @ hexy0 @ h @  + line
 hexx0 @ w @ + hexy0 @  hexx0 @  w @ + hexy0 @ h @  + line
; 



: test

\ 100 120 100 151 hexa
\ 200 120 100 151 hexa
\ attendu : x0  y0  w    h   y0+151/4 x0+150/2
\ attendu : 100 120 150 151  120+37 100+50
x0 ? y0 ?   w ? h ?   . . 
;


: hexagonize  ( x y size -- x y w h )
dup 4 * 7 / hexa
\ dup 4 * 7 / . . . .
;

: randomhex
  rnd100 rnd10 5 * + 10 + 3 * rnd100 40 + 3 * rnd100 rnd40 + hexagonize
;

: hive0 
40 4 do
50 4 do
 i 10 * j 10 * 50 hexagonize
 i 2 + 10 * j 5 + 8 * 50 hexagonize
 
4 +loop
8 +loop
;

: hive 
40 4 do
50 4 do
 i 10 * j 10 * 50 hexagonize
 i 3 + 10 * j 5 + 10 * 40 hexagonize
 
5 +loop
10 +loop
;


: draw


\ 100 120 230 130  hexa
\ 100 120 230 hexagonize

\ 200 120 230 130  hexa
\ 175 308 pix 

\ 100 1 do
 
\ randomhex

hive
 
 
 
\ loop

\ 40 25 75  hexagonize
\ 20 74 87  hexagonize

\ diamond

  flip
;

: run
  begin
    poll
    IDLE event = if draw then
  event FINISHED = until
  bye
;

: run2
 \ 2000 ms
  begin
    poll
    IDLE event = if draw 300 ms then
  event FINISHED = until
  bye
;

 
 run2

\ test
