\ --------------------------------------------
\                                           :;
\             Générateur de                 :;
\        pochettes en V (origami)           :;
\         pour vos jeux de carte            :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  
\ 
\ works with:
\   ueforth v7.0.7.18 (web only), along with origami.html to call it

\ 
\ https://eforth.arduino-forth.com

\ https://sorryweare.fr/fr/iki-en-origami-des-rangements-pratiques/





\ some forth definitions missing from default ueforth

: to-string ( n -- addr c )   <# #s #> ;

: roll  
  dup 0 <= 
  IF     drop 
  ELSE   swap >r 1- recurse r> swap 
  THEN ;


\ select default canvas size (will be changed later)

450 value ctxWidth 
150 value ctxHeight 


variable w      \ width
variable t      \ thickness
variable total  \ total length of the paper

\ different points of the schematic (from left to right)
variable p1
variable p2
variable p3
variable p4
variable p5
variable p6
variable p7
variable p8


30 CONSTANT OFFSET  \ offset from the border

defer schematic

\ size of the schematic
variable zoom
2 zoom !

\ some predefined zoom levels, redraw the schematic after changing them
: zoom1 1 zoom ! schematic ;
: zoom2 2 zoom ! schematic ;
: zoom3 3 zoom ! schematic ;
: zoom4 4 zoom ! schematic ;


: init  \ init canvas from schematic total size
 OFFSET 2 * total @ + TO ctxWidth 
 OFFSET 2 * w @ + TO ctxHeight 
 gr ctxWidth zoom @ * ctxHeight zoom @ * window
;


: line2 ( x y w -- )  \ vertical lines
 dup OFFSET zoom @ * rot w @ zoom @ * OFFSET zoom @ * + line
;


: z zoom @ * ;  \ change zoom, quicker to type


: solid       \ solid lines
 2 lineWidth
 $000000 color!   \ black
 0 0 setLineDash
;

: dashed-valley
 1 lineWidth
 $448888 color! \ green
 10 8 setLineDash
;

: dashed-mountain
 1 lineWidth
 $994433 color!  \ brown
 12 4 4 4 setLineDash4
;


: dimension   \ display dimension in mm (horizontal placement)
 5 + z  OFFSET 4 * 3 / w @ + z  fillText
;

: dimension-total
 2 - z  OFFSET 2 - z  fillText
;

: dimension-half
 z  OFFSET 2 -  z  fillText
;

: dimension-thickness  \ display on the paper
 2 + z  OFFSET 2 / w @ + z  fillText
;

: mm   \ display "mm" label
 z 15 z +  OFFSET 4 * 3 / w @ + z  s" mm " 3 roll 3 roll fillText 
;


:NONAME \ schematic

s" bold 48px serif" font 

init  

\ generate a white background
$ffffff color!
0 0 ctxWidth z ctxHeight z box

\ back to black
$000000 color!


\ calculate all the steps (8 points)
OFFSET p1 !
p1 @ w @ t @ - + p2 !
     w @ t @ - to-string p1 @ dimension  p1 @ mm
p2 @ t @ + p3 !
     \ t @ to-string p2 @ dimension-thickness \ will do it later

\ fold

p3 @ w @ + p4 !
     w @ to-string p3 @ dimension p3 @ mm
\ w @ p4 !

\ central fold
OFFSET total @ 2 / + p5 !

p5 @ w @ + p6 !
	 w @ to-string p5 @ dimension p5 @ mm
	 
p6 @ t @ + p7 !
     w @ to-string p7 @ dimension p7 @ mm  

OFFSET total @ + p8 ! 


\ draw paper background off-white 

$f7f5de color!  

p1 @ z OFFSET z  ( - )   p8 @ OFFSET - z w @  z box

$000000 color!   \ back to black color

\ draw 
	t @ to-string p2 @ dimension-thickness  
	

\ display some dimensions on top
total @ to-string p8 @ dimension-total
total @ 2 / to-string p5 @ dimension-half

\ display more dimensions on top
$a7958e color!    \ in lighter colors
s" italic 8px sans" font 
p1 @ OFFSET - to-string p1 @ dimension-half
p2 @ OFFSET - to-string p2 @ dimension-half
p3 @ OFFSET - to-string p3 @ dimension-half
p4 @ OFFSET - to-string p4 @ dimension-half
p6 @ OFFSET - to-string p6 @ dimension-half
p7 @ OFFSET - to-string p7 @ dimension-half


\ draw vertical and folding lines
solid

p1 @ zoom @ * line2

dashed-mountain

p2 @ zoom @ * line2
p3 @ zoom @ * line2

dashed-valley
p4 @ zoom @ * line2
p5 @ zoom @ * line2
p6 @ zoom @ * line2
p7 @ zoom @ * line2

solid

p8 @ zoom @ * line2

\ draw pliures

dashed-valley
p7 @ z OFFSET z p8 @ z    OFFSET w @ + z line
p3 @ z OFFSET w @ + z     p4 @ z OFFSET z line

\ draw borders

solid
p1 @ zoom @ * OFFSET zoom @ *    p8 @ zoom @ * OFFSET zoom @ * line
p1 @ z OFFSET w @ + z    p8 @ z OFFSET w @ + z line

solid


; is schematic



\ ascii version

: ascii-line ." ---------------- " cr ;

\ : cr2 space cr ;
\ : cr space 13 emit nl emit ;

: multicr cr cr cr ;



: pliure01 
."               \ " cr
."                \" cr
;

: pliure02 
."                /" cr
."               / " cr
;



: schematic-ascii
." le schéma : " cr
ascii-line
w @ t @ - cr . ." mm" multicr cr
ascii-line
t @ . ." mm" cr
ascii-line
total @ 2 / w @ - t @ - . ." mm" cr
multicr
pliure01
ascii-line
t @ . ." mm" cr
ascii-line
." pliure centrale (" total @ 2 / . ." mm)" cr
w @ . ." mm" cr 
multicr cr cr

ascii-line
t @ . ." mm" cr
ascii-line 
pliure02
w @ . ." mm" cr

multicr 

ascii-line
;


: ori ( width thickness -- )
t !
w !
w @ 4 * 100 /
t @ 2 * +
total !
testLanguage 1 = IF 
cr ." La longueur totale sera de " total @ . ." mm" cr
." le milieu pour la pliure centrale sera à " total @ 2 / . ." mm" cr
ELSE
cr ." The total length will be " total @ . ." mm" cr
." the middle for the central fold will be " total @ 2 / . ." mm" cr

THEN
cr 
\ schematic-ascii
schematic
;

: test 61 9 ori ;

: help
." ~~ HELP ~~ " CR CR 
." This page generates a pattern to make origami sleeves for your decks of cards. The final result will be a png image only. Because of its simple nature and the need to use thick paper (around 200 g), it is not necessarily relevant to print the result, this tool is to calculate more quickly the various dimensions to cut the paper. It was also a fun programming exercise." cr
." You can generate a new pattern by reloading the page, or by typing: " cr
."    width thickness ori (example: '59 16 ori')" cr
." You can add 1 mm of thickness or width so that the cards are not too tight, depending on the thickness of your paper." cr cr
." The other commands available are: zoom1, zoom2 (default), zoom3 or zoom4 to change the size of the rendering" cr cr cr cr cr cr

;

: aide
." ~~ AIDE ~~ " CR CR 
." Cette page génère un patron pour réaliser des pochettes en origami pour vos jeux de cartes. Le résultat final sera en image png uniquement. De par son caractère simple et la nécessité d'utiliser du papier épais (environ 200 g), il n'est pas forcément pertinent d'imprimer le résultat, cet outil est pour calculer plus rapidement les diverses côtes en vue de découper le papier. C'était également un exercice amusant de programmation." cr cr
." Vous pouvez générer un nouveau patron en rechargeant la page, ou en tapant : " cr
."     largeur épaisseur ori (exemple : '59 16 ori')" cr
." Vous pouvez rajouter 1 mm d'épaisseur ou de largeur pour que les cartes ne soient pas trop serrées, à adapter selon votre épaisseur de papier." cr cr
." Les autres commandes disponibles sont : zoom1, zoom2 (par défaut), zoom3 ou zoom4 pour modifier la taille du rendu" cr cr cr cr
;

: run 
testLanguage 1 = IF ." Quelle est la largeur des cartes (en mm) ? " ELSE ." How wide are the cards (in mm)? " THEN
 space 
 askWidth

 100 *
 w !
 w @ . ." mm" cr

testLanguage 1 = IF ." Quelle est l'épaisseur du paquet (en mm) ? " ELSE ." How thick is the deck (in mm)? " THEN
 space 
 askThickness
 t !
 t @ . ." mm" cr


w @ t @ ori

cr cr cr
testLanguage 1 = IF ." Tapez 'aide' pour plus d'informations." ELSE ." Type 'help' for more informations." THEN CR CR 

;



run
