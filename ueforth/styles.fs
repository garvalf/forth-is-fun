\ ANSI test Words escape   Jan Langevad 7-Jan-2025
\ Tested with ESP32FORTH
\ ANSItest.TXT   and https://pastebin.com/sHgQhgAz

: ESC ( -- ) 27 emit ; 

\ Color codes:
0 constant Black
1 constant Red
2 constant Green
3 constant Yellow
4 constant Blue
5 constant Magenta
6 constant Cyan
7 constant White

\ 0 value Pen
\ 0 value Paper

\ : Font       ( colorcode --- ) is Pen   ;
\ : Background ( volorcode --- ) is Paper ;

\ Usage e.g.:   "Red font White background" 

: CLS ( -- )    \ Clear Screen
    ESC ." [H"  \ Home to top left corner
    ESC ." [2J" \ Delete all from top left corner and down
;

: HOME ( -- )  ESC S" [H" type ;

: INT2ASCII  ( n ---)  <# # # #>  ; 

: Digit2ASCI  ( 0..9 --- char )  ;

: GOTOXY ( x y -- )  \ Move cursor to (x, y)
  ESC 
  ." [" INT2ASCII TYPE  
  ." ;" INT2ASCII TYPE 
  ." H" 
;

: FG-COLOR ( n -- )  \ 0..7 Set foreground color (30-37, as text)
  30 + 
  ESC ." [" INT2ASCII TYPE ." m" 
;

: BG-COLOR ( n -- )  \ 0..7 Set background color (40-47, as text)
  40 +
  ESC ." [" INT2ASCII TYPE ." m" 
;

:  BOLD          ESC ." [1m"  ;
:  FAINT         ESC ." [2m"  ;
: -BOLDFAINT     ESC ." [22m" ;

:  ITALIC        ESC ." [3m"  ; \ not always supported
: -ITALIC        ESC ." [23m" ;

:  UNDERLINE     ESC ." [4m"  ;
: -UNDERLINE     ESC ." [24m" ;

:  BLINK         ESC ." [5m"  ; \ not always supported
: -BLINK         ESC ." [25m" ;

:  INVERT        ESC ." [7m"  ;
: -INVERT        ESC ." [27m" ;

:  HIDDEN        ESC ." [8m"  ;
: -HIDDEN        ESC ." [28m" 
;

:  STRIKETHROUGH ESC ." [9m"  ;
: -STRIKETHROUGH ESC ." [29m" ;

: RESET          ESC ." [0m"  ; \ Reset colors and formatting


: TEST-ANSI ( -- ) 
  CLS HOME BOLD
  ." ANSI Escape Sequence Test:" CR
   5  5 GOTOXY ." Normal text" CR
   7  7 GOTOXY Red    FG-color ." Red text"    RESET CR
   9  9 GOTOXY Green  FG-Color ." Green text"  RESET CR
  11 11 GOTOXY Blue   FG-Color ." Blue text"   RESET CR
  13 13 GOTOXY Yellow FG-Color ." Yellow text" RESET CR

  15 15 GOTOXY Red    BG-Color ." Red background" RESET CR
  17 17 GOTOXY Blue   BG-Color ." Blue background" RESET CR

  BOLD          ."  This is BOLD "           -BOLDFAINT CR
  FAINT         ."  This is FAINT "          -BOLDFAINT CR
  ITALIC        ."  This is ITALIC "         -ITALIC    CR
  UNDERLINE     ."  This is UNDERLINE "      -UNDERLINE CR
  BLINK         ."  This is BLINK "          -BLINK     CR
  INVERT        ."  This is INVERT "         -INVERT    CR
  HIDDEN        ."  This is HIDDEN "         -HIDDEN ."  <--This was HIDDEN :-) " CR
  STRIKETHROUGH ."  This is STRIKETHROUGH "  -STRIKETHROUGH CR
  RESET
;

: run
TEST-ANSI
;

run

