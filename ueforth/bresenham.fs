#! /usr/bin/env ueforth 

\ Bresenham's line algorithm
\ 2023 Forth is Fun
\ https://gitlab.com/garvalf/forth-is-fun/
\ 
\ ueforth and additional code by:
\ Copyright 2022 Bradley D. Nelson
\ https://eforth.appspot.com/
\
\ Licensed under the Apache License, Version 2.0 (the "License");
\ you may not use this file except in compliance with the License.
\ You may obtain a copy of the License at
\
\     http://www.apache.org/licenses/LICENSE-2.0
\
\ Unless required by applicable law or agreed to in writing, software
\ distributed under the License is distributed on an "AS IS" BASIS,
\ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
\ See the License for the specific language governing permissions and
\ limitations under the License.

DEFINED? windows [IF] windows dpi-aware [THEN]
also graphics
also structures
also internals

struct ScanSpan
  ptr field ->next
  ptr field ->edge

4048 constant max-scanlines
create scanlines max-scanlines cells allot
scanlines max-scanlines cells erase
0 value free-edges


-1 -1 window

$ff0000 constant red
$ffff00 constant yellow
$ffffff constant white
$000000 constant black
$0000bb constant blue


variable console?   \ if console = 1, uses ascii instead of pixels
0 console? !

: pixcons  ( a b -- )
 at-xy 42 emit
;

: pixb0 ( a b -- )
   pixel 1 black fill32

;

: pixb1 ( a b -- )
   pixel 1 white fill32
;

: pixb2 ( a b -- )
   pixel 1 red fill32
;

: pixb3 ( a b -- )
   pixel 1 white fill32
;
 
\ Bresenham's algorithm
\ \\\\\\\\\\\\\\\\\\\\\

variable x
variable y
variable x0
variable y0
variable x1
variable y1
variable deltax
variable deltay
variable delta
variable yi
variable xi


variable mycolor
1 mycolor !



: lineLow ( x0 y0 x1 y1 )
\ storing data from the stack
\  cover slopes between 0 and -1 by checking 
\ whether y needs to increase or decrease (i.e. dy < 0) 
y1 ! x1 ! y0 ! x0 !  
x1 @ x0 @ - deltax !
y1 @ y0 @ - deltay !
1 yi !

deltay @ 0 <= if
-1 yi !
\ we add one to invert otherwise five invert will be minus 6
deltay @ invert 1 + deltay !  
then

deltay @ 2 * deltax @ - delta !
y0 @ y !
x0 @ x !

x1 @ x0 @ do                    \  ( loop )
\ ." x= " x ? ." y= " y ? ." delta= " delta ? cr  ( debug ) 

console? @ 1 = if
 x @ y @ pixcons else 
mycolor @ 1 = if
 x @ y @ pixb1 else
mycolor @ 2 = if
 x @ y @ pixb2 else
mycolor @ 3 = if
 x @ y @ pixb3 else
then then then then

delta @ 0 >= if
 y @ yi @ + y !
 delta @ deltay @ deltax @ - 2 * + delta !
 else
 delta @ deltay @ 2 * + delta !
then
 x @ 1 + x !
loop 
;


: lineHigh ( x0 y0 x1 y1 )
\ switching the x and y axis, 
\ for positive or negative steep slopes 
y1 ! x1 ! y0 ! x0 !  
x1 @ x0 @ - deltax !
y1 @ y0 @ - deltay !
1 xi !

deltax @ 0 <= if
-1 xi !
\ we add one to invert otherwise five invert will be minus 6
deltax @ invert 1 + deltax !  
then

deltax @ 2 * deltay @ - delta !
y0 @ y !
x0 @ x !

y1 @ y0 @ do                    \  ( for loop )
\ ." x= " x ? ." y= " y ? ." delta= " delta ? cr  ( debug ) 

console? @ 1 = if
 x @ y @ pixcons else 
mycolor @ 1 = if
 x @ y @ pixb1 else
mycolor @ 2 = if
 x @ y @ pixb2 else
mycolor @ 3 = if
 x @ y @ pixb3 else
then then then then



delta @ 0 >= if
 x @ xi @ + x !
 delta @ deltax @ deltay @ - 2 * + delta !
 else
 delta @ deltax @ 2 * + delta !
then
 y @ 1 + y !
loop 
;

: line  ( x0 y0 x1 y1 )
y1 ! x1 ! y0 ! x0 ! 
y1 @ y0 @ - abs 
x1 @ x0 @ - abs
<= if 
 x0 @ x1 @ >= if
   x1 @ y1 @ x0 @ y0 @ lineLow
 else 
   x0 @ y0 @ x1 @ y1 @ lineLow
 then
else
 y0 @ y1 @ >= if
  x1 @ y1 @ x0 @ y0 @ lineHigh
 then 
  x0 @ y0 @ x1 @ y1 @ lineHigh
then 
;


\ end of Bresenham's algorithm
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\


: garvalf 
313 267 380 200 line
380 200 343 136 line
343 136 331 61 line
331 61 270 90 line
270 90 232 49 line
232 49 223 89 line
223 89 301 105 line
301 105 223 89 line
223 89 200 93 line
200 93 193 99 line
193 99 249 106 line
249 106 301 105 line
193 99 187 113 line
187 113 228 122 line
187 113 138 117 line
138 117 228 122 line 
138 117 128 147 line 
128 147 210 198 line
128 147 175 188 line
175 188 138 192 line
175 188 210 198 line  ( mouth )
138 192 210 198 line 
138 192 133 199 line
133 199 145 216 line
145 216 215 220 line
215 220 237 249 line
237 249 313 267 line
215 220 267 225 line
267 225 313 267 line
;

: diamond

25 50 50 25 line
50 25 75 50 line 
75 50 50 75 line  
50 75 25 50 line  
;


: testconsole
25 0 do i 45 pixcons loop
40 0 do i i pixcons loop
;

: draw

 1 mycolor !
  garvalf
 2 mycolor !
 diamond
 console? @ 0 = if
  flip  \ end x window?
  else then
;

: run
  begin
  console? @ 0 = if
    poll \ start x window
    else then  
    IDLE event = if draw then
  event FINISHED = until
  bye
;

: start cr ." type 'run' to start the demo " cr ;

\ run
start