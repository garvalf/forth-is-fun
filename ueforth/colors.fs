
#! /usr/bin/env ueforth 

\ Colors
\ 2023 Forth is Fun
\ https://gitlab.com/garvalf/forth-is-fun/
\ 
\ ueforth and additional code by:
\ Copyright 2022 Bradley D. Nelson
\ https://eforth.appspot.com/
\
\ Licensed under the Apache License, Version 2.0 (the "License");
\ you may not use this file except in compliance with the License.
\ You may obtain a copy of the License at
\
\     http://www.apache.org/licenses/LICENSE-2.0
\
\ Unless required by applicable law or agreed to in writing, software
\ distributed under the License is distributed on an "AS IS" BASIS,
\ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
\ See the License for the specific language governing permissions and
\ limitations under the License.

DEFINED? windows [IF] windows dpi-aware [THEN]
also graphics
also structures
also internals

struct ScanSpan
  ptr field ->next
  ptr field ->edge

2048 constant max-scanlines
create scanlines max-scanlines cells allot
scanlines max-scanlines cells erase
0 value free-edges


-1 -1 window


variable mycolor
1 mycolor !

variable direction
1 direction !

variable rand 
here rand !

: random rand @ 31421 * 6927 + dup rand ! ;

\ rnd gives a number between 1 and 10

: rnd0 ( u1 -- u2 ) random random - random * abs 10 /mod + random + random * here + abs 10 /mod - abs 10 mod ;

: rnd ( u1 -- u2 ) random random * abs   random * here + abs 10 /mod - abs 10 mod ;

: randomcolor  random random - random * abs 10 /mod + random + random * here + abs 16777215 /mod - abs 16777215 mod  ;

\ max 16777215 for color ($ffffff)

\ change direction when the color has cycled to the max
: followflow
   mycolor @ 16777210 > if 1 direction ! else then
   mycolor @ 10 < if 0 direction ! else then
   direction @ 1 = if mycolor @ 1 - mycolor !      
    else mycolor @ 1+ mycolor !  then
;

: followflowSimple
    mycolor @ 1+ mycolor !
;

: draw

 400 40 do
  400 40 do 
  
    i rnd + j  rnd + pixel 2 mycolor @ rnd  i j * * +  fill32
  \  i rnd + j  rnd + pixel 2 mycolor @ rnd  i j s>f fcos 1000.0e f* f>s abs 1 + * + fill32
  
  \ other tests:
  
  \ i 10 - j 4 - pixel 1 randomcolor 150 / 300 + fill32
  \ i j pixel 2 randomcolor 150 / fill32
 \  i j pixel 2 mycolor @ 600 mod  fill32
 \  i j pixel 2 mycolor @ 900 mod rnd +  fill32
 \  i 10 + j  10 + pixel 2 mycolor @  fill32
   \ i rnd + j  rnd + pixel 2 mycolor @ rnd  1200 * +  fill32
  \ i rnd + j  rnd + pixel 2 mycolor @ rnd  i j * / 1000 * +  fill32
 \  mycolor @ 16777215 > if 1 mycolor ! else then
   
    
followflow
   
  \ 80 1 do loop  \ slow down
 \  mycolor @ .    \ for debug
 1 +loop 
 1 +loop
 \ rnd 2 - +loop
 
  
 flip
;



: run
  begin
    poll
    IDLE event = if draw then
  event FINISHED = until
  bye
;

 run



