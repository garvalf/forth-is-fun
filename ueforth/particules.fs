#! /usr/bin/env ueforth 

\ Particules
\ 2023 Forth is Fun
\ https://gitlab.com/garvalf/forth-is-fun/
\ 
\ ueforth and additional code by:
\ Copyright 2022 Bradley D. Nelson
\ https://eforth.appspot.com/
\
\ Licensed under the Apache License, Version 2.0 (the "License");
\ you may not use this file except in compliance with the License.
\ You may obtain a copy of the License at
\
\     http://www.apache.org/licenses/LICENSE-2.0
\
\ Unless required by applicable law or agreed to in writing, software
\ distributed under the License is distributed on an "AS IS" BASIS,
\ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
\ See the License for the specific language governing permissions and
\ limitations under the License.

DEFINED? windows [IF] windows dpi-aware [THEN]
also graphics
also structures
also internals

struct ScanSpan
  ptr field ->next
  ptr field ->edge

2048 constant max-scanlines
create scanlines max-scanlines cells allot
scanlines max-scanlines cells erase
0 value free-edges


-1 -1 window

$ff0000 constant red
$ffff00 constant yellow
$ffffff constant white
$000000 constant black
$0000bb constant blue



: pixb0 ( a b -- )
   pixel 1 black fill32

;

: pixb1 ( a b -- )
   pixel 1 white fill32
;

: pixb2 ( a b -- )
   pixel 1 red fill32
;

: pixb3 ( a b -- )
   pixel 1 white fill32
;
 
\ Bresenham's algorithm
\ \\\\\\\\\\\\\\\\\\\\\

variable x
variable y
variable x0
variable y0
variable x1
variable y1
variable deltax
variable deltay
variable delta
variable yi
variable xi


variable mycolor
1 mycolor !



variable rand 
here rand !

: random rand @ 31421 * 6927 + dup rand ! ;

: rnd ( u1 -- u2 ) random random - random * abs 10 /mod + random + random * here + abs 10 /mod - abs 10 mod ;

: randomcolor  random random - random * abs 10 /mod + random + random * here + abs 16777215 /mod - abs 16777215 mod  ;


\ max 16777215 for color ($ffffff)

: lineLow ( x0 y0 x1 y1 )
\ storing data from the stack
\  cover slopes between 0 and -1 by checking 
\ whether y needs to increase or decrease (i.e. dy < 0) 
y1 ! x1 ! y0 ! x0 !  
x1 @ x0 @ - deltax !
y1 @ y0 @ - deltay !
1 yi !

deltay @ 0 <= if
-1 yi !
\ we add one to invert otherwise five invert will be minus 6
deltay @ invert 1 + deltay !  
then

deltay @ 2 * deltax @ - delta !
y0 @ y !
x0 @ x !

x1 @ x0 @ do                    \  ( loop )
\ ." x= " x ? ." y= " y ? ." delta= " delta ? cr  ( debug ) 

mycolor @ 1 = if
 x @ y @ pixb1 else
mycolor @ 2 = if
 x @ y @ pixb2 else
mycolor @ 3 = if
 x @ y @ pixb3 else
then then then

delta @ 0 >= if
 y @ yi @ + y !
 delta @ deltay @ deltax @ - 2 * + delta !
 else
 delta @ deltay @ 2 * + delta !
then
 x @ 1 + x !
loop 
;


: lineHigh ( x0 y0 x1 y1 )
\ switching the x and y axis, 
\ for positive or negative steep slopes 
y1 ! x1 ! y0 ! x0 !  
x1 @ x0 @ - deltax !
y1 @ y0 @ - deltay !
1 xi !

deltax @ 0 <= if
-1 xi !
\ we add one to invert otherwise five invert will be minus 6
deltax @ invert 1 + deltax !  
then

deltax @ 2 * deltay @ - delta !
y0 @ y !
x0 @ x !

y1 @ y0 @ do                    \  ( for loop )
\ ." x= " x ? ." y= " y ? ." delta= " delta ? cr  ( debug ) 

mycolor @ 1 = if
 x @ y @ pixb1 else
mycolor @ 2 = if
 x @ y @ pixb2 else
mycolor @ 3 = if
 x @ y @ pixb3 else
then then then



delta @ 0 >= if
 x @ xi @ + x !
 delta @ deltax @ deltay @ - 2 * + delta !
 else
 delta @ deltax @ 2 * + delta !
then
 y @ 1 + y !
loop 
;

: line  ( x0 y0 x1 y1 )
y1 ! x1 ! y0 ! x0 ! 
y1 @ y0 @ - abs 
x1 @ x0 @ - abs
<= if 
 x0 @ x1 @ >= if
   x1 @ y1 @ x0 @ y0 @ lineLow
 else 
   x0 @ y0 @ x1 @ y1 @ lineLow
 then
else
 y0 @ y1 @ >= if
  x1 @ y1 @ x0 @ y0 @ lineHigh
 then 
  x0 @ y0 @ x1 @ y1 @ lineHigh
then 
;


\ end of Bresenham's algorithm
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\




: diamond

25 50 50 25 line
50 25 75 50 line 
75 50 50 75 line  
50 75 25 50 line  
;



: noize \ use > /dev/dsp 
dup rnd 1 + dup * / .
;

: draw

 1 mycolor !
 2 mycolor !
 
 \ diamond
 
 400 40 do
  400 40 do 
   i 10 - j 4 - pixel 1 randomcolor 150 / 300 + fill32
   i j pixel 2 randomcolor 150 / noize fill32
     80 1 do loop  \ slow down
 2 +loop 
 8 +loop
 \ rnd 2 - +loop
 
  
 flip
;

: run
  begin
    poll
    IDLE event = if draw then
  event FINISHED = until
  bye
;

run
