
# uEforth

uEforth is a small but strong FORTH for linux, windows and ESP32.

It can also displays some graphics and fits in only a few kb.



## The projects 

- [Animation with a wolf logo](anim01.html)
- [Animation Merveilles logo](merveilles.html)
- ['forthtoise': a turtle logo in ueforth](forthtoise.html)
- [Generate random patterns (glitches)](particules.html)
- [a CYOA (choose your own adventure)](cyoa.html)
- [a CYOA ("livre dont vous êtes le héros") in French](cyoa_fr.html)
- [Generate origami sleeve patterns to protect your card games (in English and French)](origami.html) 


![](ueforthtoise01.png)

![](origami01.png)

![](origami02.jpg)

![](cyoa01.png)


## More files

- [Bresenham's line algorithm](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/ueforth/bresenham.fs)
- [Project folder on Github](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/ueforth/)


## Tips 

There is a built-in visual text editor (``VISUAL EDIT file.txt``) which can be used to write text or FORTH code.

On linux document can be saved with Ctrl+P (in remplacement to Ctrl+S)

Ctrl+L can redraw the screen and Ctrl+X will save and exit.


## Links 

- https://eforth.appspot.com
- https://eforth.arduino-forth.com/


## More infos

It is derived from https://www.forth.org/eforth.html
