web definitions 
JSWORD: clearRect { x y width height }
    context.ctx.clearRect(x, y, width, height);
~
\ reset translate and rotate
JSWORD: resetTransform { }
  context.ctx.resetTransform();
~
JSWORD: getMousePosition { -- mousex mousey }
    var offset = {x: 0, y: 0};
    var node = context.ctx.canvas;
    while (node) {
        offset.x += node.offsetLeft;
        offset.y += node.offsetTop;
        node = node.offsetParent;
    }
    return [context.mouse_x-offset.x, context.mouse_y-offset.y];
~
\ detect keypress (marche pas)
JSWORD: keyPressed { }
context.ctx.canvas.onkeydown = function(e) {
    switch (e.keyCode) {
        case 37:
            alert('left');
            break;
        case 38:
            alert('up');
            break;
        case 39:
            alert('right');
            break;
        case 40:
            alert('down');
            break;
        case 65:
            alert('A');
            break;
    }
};
  //window.onkeypress = KeyPress2;
~


forth definitions

600 constant ctxWidth 
400 constant ctxHeight 

only web also forth

: mouse getMousePosition ; 
: mouse2 mouse ; 


\ clear all canvas
: cls
0 0 ctxWidth ctxHeight clearRect
;

: black $000000 color! ;

: myButton01 50 ctxHeight 30 - 80 20 ;
: myButton02 250 ctxHeight 30 - 80 20 ;
: myButton03 450 ctxHeight 30 - 80 20 ;

