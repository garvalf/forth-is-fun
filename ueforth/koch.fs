\ --------------------------------------------
\                                           :;
\                Koch  ❄                    :;
\                                           :; 
\     Turtle logo graphics for ueforth      :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  
\ see https://en.wikipedia.org/wiki/L-system#Example_4:_Koch_curve 

include ./forthtoise.fs

1200 1000 window

\ for using with https://mko.re/waforth/thurtle/
\ 2024 

HEX CDDDCA DECIMAL CONSTANT grey
grey SETSCREENCOLOR

2 SETPENSIZE

HIDETURTLE

: F 5 FORWARD ;

60 CONSTANT ANGLE 

: + ANGLE LEFT ; 
: − ANGLE RIGHT ; 

270 SETHEADING


: module01
 F + F − F − F + F + F + F − F − F + F − F + F − 
 F − F + F − F + F − F − F + F + F + F − F − F + F 
;

: demo01
 6 0 DO
module01 +
module01 −
module01 −
module01 +
module01 −
 LOOP
;

: demo02
2 0 DO
F + F − F − F + F +
RECURSE
LOOP
;

\ demo01


: draw
 450 lastX !
 250 lastY !
4 mycolor !
\ idle finished event .s drop drop drop cr 
0 oriente
2 mycolor !
 \  42 gauche 20 avance
 fvariable pi
 \ 3.14e pi f!
  demo01
flip

;

: run
  begin
    poll
      IDLE event = if draw then
    event FINISHED = until
   bye
;

