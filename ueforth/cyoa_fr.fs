
\ --------------------------------------------
\                                           :;
\                  CYOA                     :;
\                                           :; 
\   Choose your own adventure in FORTH      :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  
\ 


\ works with:
\   gforth 0.7.3->0.7.9 
\   ueforth v7.0.7.18 (cli & web)
\   DX FORTH 4.55
\   durexForth 4 


\ Select your Forth version (set it to 1):

1 CONSTANT UEFORTH
0 CONSTANT GFORTH
0 CONSTANT DUREXFORTH


: GFORTH?
GFORTH 1 = 
;

: DUREXFORTH?
DUREXFORTH 1 = 
;

: UEFORTH?
UEFORTH 1 = 
;

: init   \ initialise and check version
DUREXFORTH? IF
 s" include compat" EVALUATE \ for durexForth only
 s" : bye ; " EVALUATE
THEN
;

init

\ random nb generator

VARIABLE rnd   
HERE rnd !



\ for ueforth limitations

: MYUM* 
    UEFORTH?  IF 
       \ ." ueforth"
        1 */ ABS 0 SWAP  \ doesn't work with gforth
    ELSE 
       \ ." gforth" 
       s" UM*" EVALUATE  \ can't call um* directly for ueforth
    THEN ;  

\ end ueforth

: random  rnd @ 31421 *  6927 +  DUP rnd ! ;    \ random number (big)

: rnd10  ( u1 -- u2 ) random random MYUM* NIP 10 mod ;   
\ random number between 0 and 9

: rndn ( n_max -- u2 ) random random 1 + MYUM* NIP SWAP ( n ) mod ; 
\ random number between 0 and n

\ not used
: rnd>max ( n-max -- rnd-max ) rnd 2DUP OVER < IF 
        DROP SWAP DROP EXIT THEN 
    2DROP RECURSE  ;

: rnd>min ( n-min -- rnd-min ) rnd 2DUP OVER > IF 
        DROP SWAP DROP EXIT THEN 
    2DROP RECURSE  ;

VARIABLE answer 
VARIABLE demo


DEFER chap00   \ create words in advance so we can refer to them later
DEFER chap01
DEFER chap02
DEFER chap03
DEFER chap04
DEFER chap05
DEFER chap06
DEFER chap07
DEFER chap08
DEFER chap09
DEFER chap10


\ : compiled ." compiled!" CR ;  
: compiled ." "  ;  

: backtoforth exit ;  \ do nothing so leave

: chapini0 demo @ 1 = IF compiled EXIT THEN ;

: chapinit PAGE CR ."  ~[====> " CR ;

: theend CR ." ~ THE END ~" CR CR BYE ; 

\ this one doesn't work with ueforth:
 : ask0
\ PAD 3 ACCEPT DROP
\ 0 0 PAD 3 >NUMBER DROP DROP DROP  
\ \ convert pad to number and remove extra data in the stack
\ \  answer !   \ disable this one, use the stack

\ use S>NUMBER?  with ueforth?

;

\ this one is quicker for the player, but doesn't work well with pforth
: ask
." > "
KEY 48 -  \ key leaves ascii code, which is 49 for 1, 50 for 2 and so forth...
\ \ answer !  \ disable this one, use the stack
;



: retry CR ." Not a valid answer, please retry. " 
PAD 10 0 FILL PAD 1 accept DROP answer @ .  ;
\ : retry ; 
\ we have to clear the pad before accepting new answer


VARIABLE ?gatekey
VARIABLE ?gateopen
VARIABLE ?bucketseen
VARIABLE ?dagger
VARIABLE ?visited00
VARIABLE ?dive01

0 ?gatekey !
0 ?bucketseen !
0 ?gateopen !
0 ?dagger !
0 ?visited00 !
0 ?dive01 !

\ doesn't work, don't use that
: 0) 0 POSTPONE OF ; IMMEDIATE
: 1) 1 POSTPONE OF ; IMMEDIATE
: 2) 2 POSTPONE OF ; IMMEDIATE
: 3) 3 POSTPONE OF ; IMMEDIATE
: 4) 4 POSTPONE OF ; IMMEDIATE
: 5) 5 POSTPONE OF ; IMMEDIATE
: 6) 6 POSTPONE OF ; IMMEDIATE

\ : eof s" ENDOF" EVALUATE ; 
: eof POSTPONE ENDOF ; IMMEDIATE

: waitkey
."    [#... pressez une touche pour continuer... #] "
key drop   \ make a pause
;


:NONAME ( chap00: intro ) 
chapinit
?visited00 @ 0 = IF
CR
." @----------------------------------@ " CR
." /        le Donjon de FORTH        /" CR
." @~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-@" CR CR
." Après un long voyage de plusieurs semaines depuis votre pays d'origine," CR
." vous arrivez dans un petit jardin jouxtant une tour. "
." La tour perdue de Forth !" CR 
waitkey
."                       " CR 
."              ~        " CR 
."             /`        " CR 
."            / -`       " CR 
."           /-  -`      " CR 
."          /------`     " CR 
."          |/^` - |     " CR 
."          ||_|   |     " CR 
."          |  -   |     " CR 
."          |-   _ |     " CR 
."   _______|___|#||____ " CR 
CR
1 ?visited00 !
ELSE
." Vous êtes dans le jardin adjacent à la tour." CR
THEN
." Souhaitez-vous :" CR
." 1. explorer à gauche, vers l'étang ?" CR
." 2. explorer à droite, en s'approchant des plantes et des fleurs ?" CR
." 3. avancer vers la herse ?" CR
." 4. quitter ce lieu ?" CR
ask 
\ answer @ 
CASE
1 OF chap01 ENDOF
2 OF chap04 ENDOF
\ 3) chap05 eof
 3 OF chap05 ENDOF
4 OF chap07 ENDOF
0 OF backtoforth ENDOF
retry chap00 
ENDCASE
; IS chap00






:NONAME ( chap01: pond )
chapinit
7 rndn   \ probleme random avec version fr si 6 (ok en anglais)
CASE
0 OF ." L'étang est un endroit très calme."  CR ENDOF
1 OF ." Il y a une grenouille au bord de l'étang." CR ENDOF
2 OF ." Cette mare est assez sale, il y a de la boue qui empêche de voir le fond."
    ?dive01 @ 0 = IF  \ do you want to dive now?
            1 ?dive01 ! THEN CR ENDOF
3 OF ." Le vent déplace les herbes autour de l'étang. "  
    ?bucketseen @ 0 = IF  \ if bucket wasn't previously removed
         ." Vous découvrez un seau dans ces herbes. " 
        1 ?bucketseen ! THEN CR ENDOF
4 OF ." C'est un endroit pas très grand, vous avez envie d'aller ailleurs." CR ENDOF
." L'étang est un endroit calme. Trop calme." CR ( default )
ENDCASE

." 1. Explorer davantage cet endroit" CR
." 2. Retournez à l'entrée" CR
?bucketseen @ 1 = IF 
    ." 3. Fouiller le seau" CR THEN
?dive01 @ 1 = IF 
    ." 4. Plonger dans l'eau" CR THEN
ask
\ answer @
CASE
1 OF chap01 ENDOF 
2 OF chap00 ENDOF 
3 OF ?bucketseen @ 1 = IF 
    3 ?bucketseen ! \ remove bucket from play
    chap08 THEN retry chap01 ENDOF
4 OF ?dive01 @ 1 = IF 
    3 ?dive01 ! \ remove it
    chap06 THEN retry chap01 ENDOF
retry chap01
ENDCASE 
; IS chap01


:NONAME ( chap02 )

; is chap02


:NONAME ( chap03: inside )
chapinit
." Il y a des échos de bruits inquiétants, mais vous resserrez la poignée "
." de la dague et progressez." CR
." Qui sait toutes les aventures que vous vivrez dans cet endroit passionnant ?" 
CR
theend
; is chap03


:NONAME ( chap04: plants and flowers )
chapinit
." Vous allez à droite. "
." Ce jardin est magnifique, empli de plantes que l'on ne sait même pas nommer." CR
?gatekey @ 0 = IF
." Vous trouvez une clé dans l'herbe, que vous gardez avec vous." CR
1 ?gatekey !  \ now has key
THEN
." 1. Retourner à l'entrée du jardin." CR
ask
\ answer @
CASE
1 OF chap00 ENDOF
retry chap04
ENDCASE 
; is chap04


:NONAME ( chap05: portcullis )
chapinit
?gateopen @ 0 = IF
 ." La herse est fermée, empêchant l'accès à la porte du donjon. " CR
 ?gatekey @ 1 = IF
 ." Vous mettez la clé dans un trou de la pierre. "
 ." Il y a un mécanisme qui libère un contrepoids, "
 ." et ouvre la herse."  CR
 3 ?gatekey !  \ remove the key from play
 1 ?gateopen ! \ open the gate!
 THEN
 ELSE
  ." La herse est ouverte." CR
 THEN
." 1. Retourner à l'entrée du jardin." CR
?gateopen @ 1 = IF
  ." 2. Entrer dans la tour." CR THEN
ask
\ answer @
CASE
1 OF chap00 ENDOF
2 OF ?gateopen @ 1 = IF chap09 ELSE retry chap05 THEN ENDOF \ THEN
retry chap05
ENDCASE 
; is chap05


:NONAME ( chap06 )
." Vous plongez dans l'étang. "
." Il est boueux. Quelque chose à l'intérieur vous dévore."
theend
; is chap06


\ this chapter use a different structure for the 
\ test, it works better for older forth without CASE, but is 
\ more complicated to write

:NONAME ( chap07: leaving )
chapinit
." Vous quittez le jardin et vous approchez de la porte principale "
." du domaine." CR
." Il y a un tunnel sur la droite." CR
." 1. Retourner dans le jardin" CR
." 2. Partir une fois pour toutes" CR
." 3. Entrer dans le tunnel du FORTH" CR 
ask
 answer !  \ keep this one for the test
answer @ 1 = if chap00 else
answer @ 2 = if CR ." Vous quittez le système..." CR BYE else
answer @ 3 = if CR ." Vous êtes seul maintenant !" CR backtoforth else
retry chap07
\ ." error. "
then then then \ drop
; is chap07



:NONAME ( chap08: search bucket )
chapinit
." Il y a une dague à l'intérieur du seau. Elle émet de la lumière. "
." Vous vous en saisissez !" 
 1 ?dagger !   \ now has dagger
 CR

." 1. Continuer" CR
ask
CASE
1 OF chap01 ENDOF
retry chap08
ENDCASE
; is chap08


:NONAME ( chap09: enter dungeon )
chapinit
." Vous restez maintenant à la porte du donjon. "
." Elle est ouverte, mais mène à un couloir sombre. " CR
." 1. Continuer" CR
." 2. Retourner au jardin " CR
ask
CASE
1 OF chap10 ENDOF 
2 OF chap05 ENDOF 
retry chap09
ENDCASE 
; is chap09


:NONAME ( chap10 )
chapinit
." L'ombre couvre bientôt l'entrée lorsque vous passez devant cet endroit. "
." C'est intimidant."CR
." 1.Faire demi-tour" CR 
?dagger @ 1 = IF 
    ." 2. Avec votre poignard magique, vous osez explorer davantage." CR THEN 
ask
CASE
1 OF chap09 ENDOF
2 OF ?dagger @ 1 = IF chap03 ELSE retry chap10 THEN ENDOF
retry chap10
ENDCASE 
; is chap10


: start
0 demo !
 cr
 chap00
;

: run 
\ init  \ start it earlier!
start 
;

run
