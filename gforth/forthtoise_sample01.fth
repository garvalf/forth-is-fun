
\ --------------------------------------------
\                                           :;
\         F:O:R:T:H:T:O:I:S:E               :;
\                                           :; 
\ Turtle logo graphics for GForth and SDL2  :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\
\ using code by Robert Coffey and Remko Tronçon 
\ see   https://github.com/foggynight/gforth-turtle
\  and  https://github.com/remko/waforth
\ 
\ Copyright (C) Éric Forgeot
\ Released under the MIT license.

\ Works with libtool-bin and libsdl2*dev. 
\ Tested only on gForth 0.7.9 dev and SDL 2.0.5
\ 

require forthtoise.fth



: SQUARE ( n -- )
  4 0 DO
    DUP FORWARD
    90 RIGHT
  LOOP
  DROP
;

: PENTAGRAM ( n -- )
  18 RIGHT
  5 0 DO
    DUP FORWARD
    144 RIGHT
  LOOP
  DROP
;

: SEEKER ( n -- )
  4 0 DO
    DUP FORWARD
    PENUP
    DUP FORWARD
    PENDOWN
    DUP FORWARD
    90 RIGHT
  LOOP
  DROP
;



250 CONSTANT SIZE
7   CONSTANT BRANCHES
190 CONSTANT SPREAD

VARIABLE RND
HERE RND !

: RANDOM ( -- n )
  RND @ 75 * 74 + 65537 MOD
  DUP RND !
;

: CHOOSE ( n1 -- n2 )
  RANDOM 65537 */MOD SWAP DROP 
; 

: PLANT ( size angle -- )
  OVER 10 < IF 2DROP EXIT THEN
  DUP RIGHT
  OVER FORWARD
  BRANCHES 0 DO
    OVER 2/
    SPREAD CHOOSE SPREAD 2/ -
    rnd255 50 - ABS 153 rnd255 100 - ABS SETPENCOLOR
    RECURSE
  LOOP
  PENUP SWAP BACKWARD PENDOWN
  LEFT
;

: start-plant
  WINDOW_W 2 / WINDOW_H 2 / 250 + lastY ! lastX !
   90 LEFT
   SIZE 0 PLANT    
;


: SPIRAL ( n -- )
  DUP 1 < IF DROP EXIT THEN 
  DUP FORWARD
  15 RIGHT
  98 100 */ RECURSE
;

: CSPIRAL ( n -- )
  DUP 300 > IF DROP EXIT THEN 
  DUP 50 - 255 255 SETPENCOLOR
  DUP FORWARD
  91 RIGHT
  5 +
  RECURSE
;




: demo01
  
  26 153 0 SETPENCOLOR
  0 roundstatus !
  start-plant
  1 roundstatus !
    
  750 700 SETXY
  
  255 195 77 SETPENCOLOR
  270 oriente
  100 enneagram
  
  150 600 SETXY
  
  360 0 DO
  255 i - abs 10 i + i 2 / SETPENCOLOR
   80 SQUARE
   i LEFT
  6 +LOOP
  
  
  
   850 100 setxy
   1 CSPIRAL
  
  
  150 100 setxy
  
  
  185 195 120 SETPENCOLOR
  180 heading
  3 0 DO
    10 forward
    45 left
    40 forward
    25 right
  loop 
  
  
   185 95 60 SETPENCOLOR
  
  28 left
  70 forward
  30 backward
  30 left 
  150 forward
   

;
  

: +ou- rnd10 6 < if - else + then ;

: LeftOrRight ( n -- ) rnd10  < if LEFT else RIGHT then ;
variable originX
variable originY





:NONAME ( turtle_sketch )
  \ 8 0 do 28 SPIRAL loop
  demo01
   \ demo02
; is turtle_sketch


\ 200 110 50 !color draw-bg  

1 turtlestatus ! 

0 slowmo !
5 waittime !
run 
 
