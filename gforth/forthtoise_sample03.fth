
\ --------------------------------------------
\                                           :;
\         F:O:R:T:H:T:O:I:S:E               :;
\                                           :; 
\ Turtle logo graphics for GForth and SDL2  :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\
\ using code by Ashley Nathan Feniello
\ see   https://stackoverflow.com/questions/12778187/examples-of-very-concise-forth-applications
\ and  https://web.archive.org/web/20150703001329/http://blogs.msdn.com/b/ashleyf/archive/2012/02/18/turtle-graphics-on-the-fignition.aspx

\ this will display a little demo with several animations 

\ 
\ Copyright (C) Éric Forgeot
\ Released under the MIT license.

\ Works with libtool-bin and libsdl2*dev. 
\ Tested only on gForth 0.7.9 dev and SDL 2.0.5
\ 

require forthtoise.fth

: var variable ;

0 var x 0 var y 0 var a
0 var q 0 var w 

: c cls ; \ clear screen
\ : k >r 50 + 8 << r> ! ;
: m PENUP FORWARD  PENDOWN ; \ move n-pixels (without drawing)
: g WINDOW_W 2 / WINDOW_H 2 / SETXY ; \ go to x,y coord
: h 6 * SETHEADING ; \ heading
: f FW ; \ forward n-pixels
: e   ; \ end
: b  ; \ begin
: t 6 * RIGHT ; \ turn n-degrees
: s s>f fdeg>rad fsin 100e f* f>s ;


\ 8.8 fixed point sine table lookup
\ -2 var n F9F2 , E9DD , CEBD , AA95 , 7F67 , 4E34 , 1A c,
\ : s abs 3C mod dup 1D > if 3C swap - then dup E > if -1 1E rot - else 1 swap then n + c@ 1+ * ;

: sinus 1600 0 do i i s 2 / 80 + draw-point loop ;

: burst  60 0 do 0 0 g i h 110 f loop ;

: squiral -50 50 g 20 0 do 100 f 21 t loop ;

: circle 60 0 do 4 f 1 t loop ; 
: spiral 15 0 do circle 4 t loop ;

: rose 0 50 0 do 2 + dup f 14 t loop ;

: hp 15 0 do 5 f 1 t loop 15 0 do 2 f -1 t loop ;
: petal hp 30 t hp 30 t ;
: flower 15 0 do petal 4 t loop ;


: star 5 0 do 80 f 24 t loop ;
: stars 3 0 do star 20 t loop ;


: refresh  500 waittime ! 3 0 do 500 wait loop cls 0 0 g 5 waittime ! rnd255 rnd255 rnd255 !color ;

: demo01
\  300 400 SETXY
HIDETURTLE
 rnd255 rnd255 rnd255 !color
sinus refresh
burst refresh
rose refresh
squiral  refresh
spiral  refresh
stars refresh
flower \ refresh


;
  




:NONAME ( turtle_sketch )
  \ 8 0 do 28 SPIRAL loop
  demo01
   \ demo02
; is turtle_sketch


\ 200 110 50 !color draw-bg  

1 turtlestatus ! 

1 slowmo !
5 waittime !
run 
 
