
\ happy new year
\
\ Display a little banner with some snow flakes
\ Made for gforth https://gforth.org/

$7E8 constant CURRENTYEAR 

variable rand 
here rand !

: random rand @ 31421 * 6927 + dup rand ! ;

: rnd10 ( u1 -- u2 ) random random - random * abs 10 /mod + random + random * here + abs 10 /mod - abs 10 mod ;

: pixel00 42 emit ;
: pixel0 ." 🮋" ;
: pixel 
 rnd10 
  case
  1 of ." ❄" endof
  2 of ." ❄" endof
  3 of ." ❅" endof
  4 of ." ❅" endof
  5 of ." ❆" endof
  6 of ." ❆" endof
  7 of ." ❇" endof
  8 of ." ❇" endof
  >r ." ❄" r>
  endcase
;

: testsnow 8 > if pixel else space then ; 



: snow
cr
5 1 do
 29 1 do rnd10 dup testsnow 
 loop cr
 29 1 do testsnow loop
 cr
loop
 29 1 do rnd10 3 > if pixel else space then loop cr
 29 1 do rnd10 2 > if pixel else space then loop cr
;

: line 29 1 do pixel loop ;


: displayyear CURRENTYEAR 10 base ! u. decimal ; 


: bonneannee cr cr snow line cr ." Bonne et heureuse année " displayyear cr line cr cr ;

: happynewyear cr cr snow line cr ." **  Happy new year  " displayyear ."  ** " cr line cr cr ;


happynewyear

