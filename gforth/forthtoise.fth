
\ --------------------------------------------
\                                           :;
\         F:O:R:T:H:T:O:I:S:E               :;
\                                           :; 
\ Turtle logo graphics for GForth and SDL2  :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\
\ using code by Robert Coffey and Remko Tronçon 
\ see   https://github.com/foggynight/gforth-turtle
\  and  https://github.com/remko/waforth
\ see also https://mathscitech.org/articles/turtle-logo-forth
\ 
\ Copyright (C) Éric Forgeot
\ Released under the MIT license.

\ Works with libtool-bin and libsdl2*dev. 
\ Tested only on gForth 0.7.9 dev and SDL 2.0.5
\ 
\ 
\ To use it, create a new word, and call it 
\  from the "(main)" word
\ 
\ WORDS: 
\
\   FORWARD ( n -- ): Move forward by n.
\   BACKWARD ( n -- ): Move backward by n.
\   LEFT ( n -- ): Turn left by n degrees.
\   RIGHT ( n -- ): Turn right by n degrees.
\   SETXY ( n1 n2 -- ): Move to position n1,n2.
\   SETHEADING ( n -- ): Set heading n degrees clockwise from Y axis.
\   PENUP ( -- ): Disable drawing while moving.
\   PENDOWN ( -- ): Enable drawing while moving.
\   SETPENSIZE ( n -- ): Set the width of the drawed strokes to n (default: 1). (NOTE: you can't change it yet)
\   SETPENCOLOR ( R G B -- ): Set the color of the drawed strokes to RGB value n (default: 255 255 255).
\   HIDETURTLE ( -- ): Hide the turtle.
\   SHOWTURTLE ( -- ): Show the turtle.
\

require sdl_init.fth


\ Variables

variable penstatus   \ 1 = pen down / 0 = pen up
1 penstatus !

variable turtlestatus   \ 1 = turtle shown / 0 = turtle hidden
1 turtlestatus !

variable roundstatus   \ 1 = correct rounding angles / 0 = no correction
1 roundstatus !

variable slowmo   \ 1 = enable step by step / 0 = render all at once
1 slowmo !

variable waittime   \ time of delay for slow motion mode
60 waittime !

variable hexx0
variable hexy0
variable p1x  \ top of hexagon x
variable p1y  \ top of hexagon y
variable p2x  \ bottom of hexagon x
variable p2y  \ bottom of hexagon y
variable w
variable h



variable r1
variable r2
variable r3
variable r4

variable fgColor

variable rand
here rand !
variable randx

variable lastX
variable lastY

\ float variables
fvariable angle
fvariable length


variable replay  \ how many time do we replay the main scene
1 replay !

\ extra variable for examples

VARIABLE n1
VARIABLE n2
VARIABLE size
VARIABLE density
VARIABLE décalagex
VARIABLE décalagey
VARIABLE nb
VARIABLE décalage


: line ( x0 y0 x1 y1 -- ) penstatus @ 1 = if draw-line else drop drop drop drop then ;


\ Random numbers

: random rand @ 31421 * 6927 + dup rand ! ;

: rnd100 ( u1 -- u2 ) random random - random * abs 100 /mod + random + random * here + abs 100 /mod - abs 100 mod 1+ ;

: rnd255 ( u1 -- u2 ) random random - random * abs 255 /mod + random + random * here + abs 255 /mod - abs 255 mod 1+ ;

: rnd40 ( u1 -- u2 ) random random - random * abs 40 /mod + random + random * here + abs 40 /mod - abs 40 mod 1+ ;
: rnd10 ( u1 -- u2 ) random random - random * abs 10 /mod + random + random * here + abs 10 /mod - abs 10 mod 1+ ;

: rndX randx ! random random UM* NIP randx @ mod 1+ ;

: randomcolor  random random - random * abs 10 /mod + random + random * here + abs 16777215 /mod - abs 16777215 mod  1+ ;






: fdeg>rad ( f: x -- f: x )  pi f* 180e f/ 
;



: angle-check
 angle f@ f>s .
;



: wait  ( t -- ) 
  \ timer100000   \ slowest
  \ timer10000
  \ timer1000    
\ timer500      \ faster
   timer
;

\ Turtle LOGO


: checkround   \ round toward nearest
roundstatus @ 1 = if
  100e f* f>s 100 /mod swap 50 < if else 1 + then   \ correction
 \ f>s
 else
   f>s                                             \ no correction
 then
;

: FORWARD ( n -- )  \ Move forward by n.
 s>f \ length f!
lastX @ lastY @ angle f@ fdeg>rad fcos fswap ( length -- ) fdup frot f* checkround lastX @ + dup lastX !
                angle f@ fdeg>rad fsin fswap ( length -- ) 
                \ ." f :" f.s  \ debug
                f* checkround lastY @ + dup lastY ! 
                \ .s cr     \ debug
                line
                slowmo @ 1 = if waittime @ wait else then
;


\ older version, using 2 variables

: FORWARD0 ( n -- )  \ Move forward by n.
s>f length f!
lastX @ lastY @ angle f@ fdeg>rad fcos length f@ f* checkround lastX @ + dup lastX !
                angle f@ fdeg>rad fsin length f@ f* checkround lastY @ + dup lastY ! line
                slowmo @ 1 = if waittime @ wait else then ;


: BACKWARD ( n -- )   \  Move backward by n. 
 negate FORWARD
;

: RIGHT ( n -- )     \  Turn right by n degrees angle.
 s>f angle f@ f+ ( fabs ) angle f!
;


: LEFT ( n -- )   \  Turn left by n degrees angle.
\ s>f angle f@ f-  angle f!
 negate right
;

\ TODO: check 
: SETXY ( n1 n2 -- )   \ Move to position n1,n2.
 lastX @ lastY @ 2dup 2swap line lastY ! lastX ! ; 

: SETHEADING  ( n -- )  \ Set heading n degrees clockwise from Y axis.
 s>f angle f!
;

: PENUP 0 penstatus ! ; 
: PENDOWN 1 penstatus ! ; 
: HIDETURTLE 0 turtlestatus ! ;   \ 
: SHOWTURTLE 1 turtlestatus ! ;
: SETPENCOLOR ( R G B -- )  \ Set the color of the drawed strokes to RGB value n (default: 0)
\ 13047173 = #C71585
!color 
; 


\ TODO 
: SETPENSIZE ;   \ might work with SDL_gfx see https://stackoverflow.com/questions/21560384/how-to-specify-width-or-point-size-in-sdl-2-0-draw-points-lines-or-rect


\ Synonyms


: heading SETHEADING ;  \ ( n -- ): Set heading n degrees clockwise from Y axis.
: hidepen HIDETURTLE ;
: showpen SHOWTURTLE ;

: lt LEFT ;
: rt RIGHT ;
: fw FORWARD ;
: bk BACKWARD ; 
: color SETPENCOLOR ;

\ weForth Logo synonyms

: FD FORWARD ;
: LT LEFT ;
: RT RIGHT ;
: BK BACKWARD ;
: HT HIDETURTLE ;
: ST SHOWTURTLE ;
: CS cls ;  \ clear screen
: CT ;  \ undo (rewind)
: PD PENDOWN ; 
: PU PENUP ;  
: PC SETPENCOLOR ;  \ pen color
: FG SETPENCOLOR ;  \ foreground color 
: BG ;  \ background color (not implemented)
: HD SETHEADING ;    \ orientation
: PW SETPENSIZE ;    \ not implemented
: XY  SETXY ; 

\ French translation

: avance FORWARD ;      \ ( n -- ): Move forward by n.
: recule BACKWARD ;     \ ( n -- ): Move backward by n.
: gauche LEFT ;         \ ( n -- ): Turn left by n degrees.
: droite RIGHT ;        \ ( n -- ): Turn right by n degrees.
: aller SETXY ;         \ ( n1 n2 -- ): Move to position n1,n2.
: oriente SETHEADING ;  \ ( n -- ): Set heading n degrees clockwise from Y axis.
: lève    PENUP ;       \ ( -- ): Disable drawing while moving.
: pose    PENDOWN ;     \ ( -- ): Enable drawing while moving.
: crayon  ;
: taille SETPENSIZE ;   \ ( n -- ): Set the width of the drawed strokes to n (default: 5).
: cache HIDETURTLE ;    \ ( -- ): Hide the turtle.
: montre SHOWTURTLE ;   \ ( -- ): Show the turtle.
: tortue  ;



\ tests and samples

: enneagram ( size -- ) \ 9 angles star
n1 !
\ 270 oriente
9 0 DO
n1 @ avance
65 gauche
n1 @ avance
145 droite
LOOP
;




: turtle-logo0
\ Art by Joan Stark
cr cr 
."                    __   " cr
."         .,-;-;-,. /'_\  " cr
."       _/_/_/_|_\_\) /   " cr
."     '-<_><_><_><_>=/\   " cr
."       `/_/====/_/-'\_\  " cr
."        ''     ''    ''  " cr 
cr ." ~ FORTHTOISE 🐢 ~" cr cr
;

: turtle-logo
cr cr
."   _____     ____   " cr
."  /      \  |  o |  " cr 
." |        |/ ___\|   " cr
." |_________/        " cr
." |_|_| |_|_|          " cr 
cr ." ~ FORTHTOISE 🐢 ~" cr cr
cr ." Type 'run' to start the rendering " cr cr 
;


: turtle-icon-simple
38 230 0 SETPENCOLOR
PENUP 0 RIGHT 8 FORWARD  PENDOWN 90 LEFT 
\ 12 FORWARD 120 RIGHT 26 FORWARD 120 RIGHT 26 FORWARD 120 RIGHT 12 FORWARD 180 RIGHT
9 FORWARD 120 RIGHT 20 FORWARD 120 RIGHT 20 FORWARD 120 RIGHT 9 FORWARD 180 RIGHT
\ 90 LEFT 5 FORWARD 90 LEFT 1 FORWARD 90 LEFT 5 FORWARD 90 LEFT 2 FORWARD 90 LEFT 5 FORWARD
90 LEFT 1 FORWARD 90 LEFT 5 FORWARD 10 BACKWARD 5 FORWARD 90 LEFT 5 FORWARD

;


: turtle-icon-simple0

38 230 0 SETPENCOLOR
PENUP 0 RIGHT 10 FORWARD  PENDOWN 90 LEFT

\ BODY: 
12 FORWARD 120 RIGHT 26 FORWARD 120 RIGHT 26 FORWARD 120 RIGHT 12 FORWARD 180 RIGHT

\ HEAD: 
PENUP 90 RIGHT 28 BACKWARD 90 LEFT 0 FORWARD 90 RIGHT PENDOWN 
45 RIGHT

4 0 DO  8 FORWARD 90 LEFT 8 FORWARD 90 LEFT LOOP

45 LEFT

\ TAIL :
PENUP  30 FORWARD  90 LEFT 4 FORWARD  PENDOWN  55 LEFT 
3 0 DO   120 LEFT 7 FORWARD LOOP
;


: turtle-icon

38 230 0 SETPENCOLOR

90 LEFT
PENUP 4 FORWARD 90 RIGHT 4 FORWARD PENDOWN 
90 RIGHT

\ BODY :
2 0 DO 
 10 FORWARD 45 LEFT 8 FORWARD 45 LEFT  17 FORWARD  45 LEFT 8 FORWARD 45 LEFT
LOOP


\ HEAD: 
PENUP 2 FORWARD 90 RIGHT 27 BACKWARD 90 LEFT 6 FORWARD 90 RIGHT PENDOWN

195 RIGHT

2 0 DO
 7 FORWARD 60 LEFT 7 FORWARD 90 LEFT
LOOP

\ TAIL :
PENUP 105 RIGHT 28 FORWARD PENDOWN

270 LEFT
3 0 DO  
 120 LEFT 5 FORWARD
LOOP

\ PAWS:

PENUP 1 FORWARD 90 RIGHT 3 FORWARD PENDOWN
120 LEFT 5 FORWARD 85 RIGHT 4 FORWARD 85 RIGHT 3 FORWARD
PENUP 60 LEFT 20 FORWARD 120 RIGHT 2 BACKWARD PENDOWN
120 LEFT 3 FORWARD 120 RIGHT 6 FORWARD 20 LEFT 0 FORWARD
PENUP 17 FORWARD PENDOWN
0 FORWARD 20 LEFT 6 FORWARD 120 RIGHT 3 FORWARD 120 LEFT
PENUP 124 RIGHT 21 FORWARD PENDOWN
70 LEFT 3 FORWARD 85 RIGHT 4 FORWARD 85 RIGHT 3 FORWARD

;

defer turtle_sketch
  


: (main)
  \ begin
  ( replay -- ) 0 do
    begin event SDL_PollEvent  while
      event SDL_Event-type @ 0xFFFFFFFF and { et }
      et SDL_QUIT = if SDL_Quit exit then
    repeat
    
    \ init screen 
     0 0 0 !color draw-bg  \ clear screen
     WINDOW_W 2 / WINDOW_H 2 / lastY ! lastX !   \ make center of the screen the start of the drawing
     255 255 255 !color   \ default color to white
     
    \ call your word from here: 

      \ demo01
      \ demo02
      turtle_sketch  
   \ display turtle or not
   
   
   turtlestatus @ 1 = IF  turtle-icon-simple
     \ turtle-icon
      ELSE THEN
   
    present [ 10000 FRAMES/SECOND / ] literal SDL_Delay
  \ false until 
  loop
;
  
: run  replay @ init (main) ;


turtle-logo

:NONAME ( turtle_sketch )
  32 0 do 
  45 left
  i 8 * 255 i 5 * -  100 color
  120 enneagram 
  4 +loop
; is turtle_sketch




\ comparison with https://mko.re/waforth/thurtle/
\ status : 
\  square :    working  
\  square (w/ LOOP) : working
\  Crooked Outward Square Spiral : working
\  Pentagram : working
\  seeker : working
\  flower : working
\  spiral : working

\  snowflake : not working
\ SETXY : doesn't use PENDOWN when moving to location
\ SETPENCOLOR : uses 3 values (R G B) instead of just one
\ SETPENSIZE : doesn't work (SDL_render limitation)
