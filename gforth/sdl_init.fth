\ SDL initialisation 

table >order definitions
require lib/SDL.fth
require lib/SDL_events.fth
require lib/SDL_render.fth
require lib/SDL_timer.fth
require lib/SDL_video.fth
wordlist >order definitions

\ 800 constant WINDOW_W 600 constant WINDOW_H
1024 constant WINDOW_W 768 constant WINDOW_H

144 constant FRAMES/SECOND


variable window
variable render
variable t

\ Init more SDL

create event SDL_Event allot

: init-sdl2   SDL_INIT_VIDEO SDL_Init drop ;
: init-window
  s\" 🐢 Forthtoise \0" drop
  SDL_WINDOWPOS_UNDEFINED SDL_WINDOWPOS_UNDEFINED
  WINDOW_W WINDOW_H SDL_WINDOW_SHOWN
  SDL_CreateWindow window ! ;
: init-render   window @ -1 SDL_RENDERER_ACCELERATED SDL_CreateRenderer render ! ;
: init   init-sdl2 init-window init-render ;

: !color { r g b -- }   render @ r g b 255 SDL_SetRenderDrawColor drop ;
: draw-bg   render @ SDL_RenderClear drop ;
: draw-line ( x0 y0 x1 y1 -- ) render @  4 0 do  4 roll loop SDL_RenderDrawLine drop ;
: draw-point ( x y -- )   render @ rot rot SDL_RenderDrawPoint drop ;

\ only for dots, not for lines :( : 
: scale-point ( scale -- )  render @ swap s>f fdup SDL_RenderSetScale drop ;

: present   render @ SDL_RenderPresent ;



: filledRect ( x0 y0 -- ) render @ 50 75  SDL_RenderFillRect drop  ;
: circle ( x0 y0 x1 y1 -- ) render @  4 0 do  4 roll loop SDL_RenderDrawLine drop  ;
 \ see https://stackoverflow.com/questions/38334081/how-to-draw-circles-arcs-and-vector-graphics-in-sdl



: cls 0 0 0 !color draw-bg ;

: timer0 t ! present [ t @ FRAMES/SECOND / ] literal SDL_Delay ;
: timer render @  SDL_RenderPresent ( -- ) SDL_Delay ;
: timer100000  present [ 100000 FRAMES/SECOND / ] literal SDL_Delay ;
: timer10000  present [ 10000 FRAMES/SECOND / ] literal SDL_Delay ;
: timer1000  present [ 1000 FRAMES/SECOND / ] literal SDL_Delay ;
: timer500  present [ 500 FRAMES/SECOND / ] literal SDL_Delay ;
: timer200  present [ 200 FRAMES/SECOND / ] literal SDL_Delay ;

