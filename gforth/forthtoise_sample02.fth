
\ --------------------------------------------
\                                           :;
\         F:O:R:T:H:T:O:I:S:E               :;
\                                           :; 
\ Turtle logo graphics for GForth and SDL2  :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\
\ using code by Robert Coffey and Remko Tronçon 
\ see   https://github.com/foggynight/gforth-turtle
\  and  https://github.com/remko/waforth
\ 
\ Copyright (C) Éric Forgeot
\ Released under the MIT license.

\ Works with libtool-bin and libsdl2*dev. 
\ Tested only on gForth 0.7.9 dev and SDL 2.0.5
\ 

require forthtoise.fth

variable originX
variable originY



: +ou- rnd10 6 < if - else + then ;

: LeftOrRight ( n -- ) rnd10  < if LEFT else RIGHT then ;



: head
rnd10 1+ 2 * 0 do
35 left 
rnd10 1+ forward
loop
;


: tige ( panning_0-10 size -- )
originX @ rnd10 +ou- originY @ setxy
270 oriente penup 115 fw pendown
swap
8 1 do 
 2dup
  rnd10 1 + ( panning -- ) LeftOrRight 
  ( size -- ) rndX 10 1+ + fw
  loop
  drop drop
  head
;





: demo02 ( plant )

0 oriente 

HIDETURTLE
penup
90 rt 300 fw lastY @ lastX @ originX ! originY !
90 lt
pendown

\ pot 
25 fw
75 lt
120 fw
105 lt
100 fw
105 lt
120 fw
75 lt 
17 fw
90 lt

\ flower 

25 150 40 color


1 40 tige
5 50 tige
5 30 tige
8 65 tige
10 35 tige

10 1 do
 rnd10 rnd10 1+ 2 / 10 * tige
 loop

  
;




:NONAME ( turtle_sketch )
  \ 8 0 do 28 SPIRAL loop
  \ demo01
   demo02
; is turtle_sketch


200 110 50 !color draw-bg  

1 turtlestatus !


10 replay !

1 slowmo !
50 waittime !

run 
 
