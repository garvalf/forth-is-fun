
# Gforth

Gforth is a modern FORTH for many systems.


## The projects 

- [A turtle for gForth](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/gforth/forthtoise.fth)
- [Project folder on Github](https://gitlab.com/garvalf/forth-is-fun/-/tree/main/gforth/)


![](forthtoise01.png)


## Tips 

For graphical effects, install libtool-bin and libsdl2*dev. 
Tested on gForth 0.7.3, gForth 0.7.9-dev and SDL 2.0.5


## Links 

- https://gforth.org
 


## More infos
