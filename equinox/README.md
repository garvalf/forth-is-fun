# Equinox


Equinox is a Forth programming language that targets Lua and love2d


## Installation

```
sudo apt install love
sudo apt install lua5.1
sudo apt install liblua5.1-0-dev
sudo apt install luarocks
sudo luarocks-5.1 install equinox
```

(you can use ``sudo luarocks-5.xx install equinox`` to force using a version of luarocks matching your lua / love2d installation)

 

## Running the example

to launch the example, just type:

``love ./``


(love2d and equinox must be installed)



## Links 

- https://github.com/zeroflag/equinox
