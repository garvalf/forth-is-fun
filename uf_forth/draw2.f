
( dumb pseudo line generation )
	
: pix3 ( a b -- )
 position 
 43 pixel 
;

: pix2 ( a b -- )
 position 
 42 pixel 
;

: pix1 ( a b -- )
 position 
 41 pixel 
;

: pix
pix2
;

: line01
100 0 do 
150 i +
240 i -
pix
loop
;

: line02 
100 0 do 
150 i +
240 i 2 * +
pix
2 +loop
;




( create ax 10 cells allot )

variable ax
variable ay
variable bx
variable by

variable mx
variable my

variable mousex
variable mousey

138 mousex !
117 mousey !


: cross  ( to be used within line )
ax @ 1 + ay @ 1 + pix
bx @ 1 + by @ 1 + pix

ax @ 1 - ay @ 1 - pix
bx @ 1 - by @ 1 - pix

ax @ 1 - ay @ 1 + pix
bx @ 1 - by @ 1 + pix

ax @ 1 + ay @ 1 - pix
bx @ 1 + by @ 1 - pix
;

: find-middle
+ 2 / 
mx !
+ 2 /
my !

mx @ my @ pix

mx @ ax @ + 2 / 
my @ ay @ + 2 / 
pix3

mx @ bx @ + 2 / 
my @ by @ + 2 / 
pix3

;


: find-middle0
ax @ bx @ + 2 / 
mx !
ay @ by @ + 2 /
my !

mx @ my @ pix

mx @ ax @ + 2 / 
my @ ay @ + 2 / 
pix3

mx @ bx @ + 2 / 
my @ by @ + 2 / 
pix3
;

: find-middle00
( find the middle )
ax @ bx @ + 2 / 
ay @ by @ + 2 /
pix
( find the middle of the middle )
ax @ bx @ + 2 /
bx @ + 2 /
ay @ by @ + 2 / 
by @ + 2 /
pix3
( find the 3rd middle )
ax @ bx @ + 2 /
ax @ + 2 /
ay @ by @ + 2 / 
ay @ + 2 /
pix3
;


: line ( ax ay bx by -- ) 
( remember to reverse data order from the stack! )
by !
bx !
ay !
ax !
ax @ ay @ pix
bx @ by @ pix

( comment or not )
 cross 

by @ ay @ bx @ ax @ 
find-middle

my @ ay @ mx @ ax @ 
find-middle 

( we do that to the other side as well )
by @ ay @ bx @ ax @ 
 find-middle 

my @ by @ mx @ bx @ 
 find-middle  

;



: test
140 250 170 230 line
0 150 30 410 line
;


: run
313 267 380 200 line
380 200 343 136 line
343 136 331 61 line
331 61 270 90 line
270 90 232 49 line
232 49 223 89 line
223 89 301 105 line
301 105 223 89 line
223 89 200 93 line
200 93 193 99 line
193 99 249 106 line
249 106 301 105 line
193 99 187 113 line
187 113 228 122 line
187 113 138 117 line
( 138 117 228 122 line )
( 138 117 128 147 line )
mousex @ mousey @ 228 122 line
mousex @ mousey @ 128 147 line
128 147 210 198 line
128 147 175 188 line
175 188 138 192 line
175 188 210 198 line  ( mouth )
138 192 210 198 line 
138 192 133 199 line
133 199 145 216 line
145 216 215 220 line
215 220 237 249 line
237 249 313 267 line
215 220 267 225 line
267 225 313 267 line
;

h# 1b75 h# 2da6 h# 3db8 colors


\ begin
\ mouse mousey ! mousex !
\ run
\ again

\ save draw2.rom
\ bye 

page
run
