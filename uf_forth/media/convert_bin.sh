

# use this for uxntal binary code:
# xxd -p -g 2 -c 8 $1 | sed 's/../& /g'

# use this for UF Forth binary code:
FILE=$1
printf "create ${FILE%%.*} hex \n" > "${FILE%%.*}".f
xxd -p -g 2 -c 8 $1 | sed 's/../& c, /g' >> "${FILE%%.*}".f
printf "\n decimal\n\n" >>  "${FILE%%.*}".f