\ draw simple rectangles 
\ for UF Forth 

\ draw UF Forth logo 

\ SRC: https://gitlab.com/garvalf/forth-is-fun

variable x
variable y
variable h
variable w
variable x0
variable y0

variable mousex
variable mousey

variable ystart

138 mousex !
117 mousey !


: pix ( a b -- )
 position 
 41 pixel 
;


: pix0 ( a b -- )
 position 
 40 pixel ( black )
;

\ TODO : swap the rect and box words? 
\ it seems more logical to define a box by a size...

: rect ( x0 y0 w h )
h ! w ! y0 ! x0 !
x0 @ x !
y0 @ y !
w @ -1 do     \ loop begin
  x @ y0 @ pix
  x @ y0 @ h @ + pix
  x @ 1 + x !
loop 
h @ -1 do    \ loop begin
  x0 @ y @ pix
  x0 @ w @ + y @ pix
  y @ 1 + y !
loop
;

: box  ( x0 y0 x1 y1 )
3 pick 3 pick
rot swap -
rot rot - swap
rect
;

: button ( x0 y0 x1 y1 )
4 0 do 3 pick loop
4 0 do 3 pick 2 + loop
\ box 
over 3 pick moveto lineto 
drop over 2 + lineto \ shadow
3 pick 2 + 3 pick 2 + 
3 pick 2 - 3 pick 2 - pix pix  \ dots
 box
pix0 pix0   \ black dots
;

: testbutton 
15 60 215 110 
;

: testbox
15 10 200 100 rect  \ top left, w h
15 10 215 110 box   \ top left, bottom right
;

: cls
page
512 0 do 
320 0 do
j i  pix0
loop loop
;



( ---------------------- for test only )

variable ax
variable ay
variable bx
variable by

variable mx
variable my



: find-middle
+ 2 /  mx ! + 2 / my !
mx @ my @ pix
mx @ ax @ + 2 / 
my @ ay @ + 2 / 
pix

mx @ bx @ + 2 / 
my @ by @ + 2 / 
pix
;




: line0 ( ax ay bx by -- ) 
( remember to reverse data order from the stack! )
by ! bx ! ay ! ax !
ax @ ay @ pix
bx @ by @ pix

by @ ay @ bx @ ax @ 
find-middle

my @ ay @ mx @ ax @ 
find-middle 

( we do that to the other side as well )
by @ ay @ bx @ ax @ 
 find-middle 

my @ by @ mx @ bx @ 
 find-middle  
;





\ demo 

\ 50 60 100 140 rect

: demo2
200 40 do
300 50 do
100 i + 150 j + i j rect
( i @ 4 + i ! )
( j @ 4 + j ! )
4 +loop 6 +loop 
;

: demo3
100 140 150 150 rect
105 145 140 140 rect
110 150 130 130 rect
270 70 40 50 rect
;


: demo4
rnd abs 100 / 
rnd abs 100 /
rnd abs 180 /
rnd abs 180 /
rect
( .s )
;



: demo5
40 80 12 75 rect
68 80 12 75 rect
47 149 25 12 rect 

94 80 12 81 rect
94 80 40 12 rect
94 107 32 12 rect

202 80 12 81 rect
202 80 40 12 rect
202 107 32 12 rect

256 87 12 67 rect
283 87 12 67 rect
263 80 25 12 rect
263 148 25 12 rect

303 80 34 12 rect
303 115 34 12 rect
303 80 12 81 rect
331 87 12 34 rect
325 127 12 6 rect
330 133 12 13 rect
337 147 12 13 rect

365 80 40 12 rect
378 80 12 81 rect

418 80 12 81 rect
446 80 12 81 rect 
418 108 40 12 rect
;


\ UF Forth Logo
: demo6
40 80 ystart @ + 12 75 rect
68 80 ystart @ + 12 75 rect
47 149 ystart @ + 25 12 rect 

94 80 ystart @ + 12 81 rect
94 80 ystart @ + 40 12 rect
94 107 ystart @ + 32 12 rect

202 80 ystart @ + 12 81 rect
202 80 ystart @ + 40 12 rect
202 107 ystart @ + 32 12 rect

256 87 ystart @ + 12 67 rect
283 87 ystart @ + 12 67 rect
263 80 ystart @ + 25 12 rect
263 148 ystart @ + 25 12 rect

303 80 ystart @ + 34 12 rect
303 115 ystart @ + 34 12 rect
303 80 ystart @ + 12 81 rect
331 87 ystart @ + 12 34 rect
325 127 ystart @ + 12 6 rect
330 133 ystart @ + 12 13 rect
337 147 ystart @ + 12 13 rect

365 80 ystart @ + 40 12 rect
378 80 ystart @ + 12 81 rect

418 80 ystart @ + 12 81 rect
446 80 ystart @ + 12 81 rect 
418 108 ystart @ + 40 12 rect
;

: demo7
10 1 do demo4 loop ; 

: dice1
cls
100 100 100 100 rect
145 145 10 10 rect
;

: dice2
cls
100 100 100 100 rect
110 180 10 10 rect
180 110 10 10 rect
;

: dice3
cls
100 100 100 100 rect
110 180 10 10 rect
180 110 10 10 rect
145 145 10 10 rect
;

: dice4
cls
100 100 100 100 rect
110 180 10 10 rect
180 110 10 10 rect
110 110 10 10 rect
180 180 10 10 rect
;

: dice5
cls
100 100 100 100 rect
110 180 10 10 rect
180 110 10 10 rect
110 110 10 10 rect
180 180 10 10 rect
145 145 10 10 rect
;

: dice6
cls
100 100 100 100 rect
110 180 10 10 rect
180 110 10 10 rect
110 110 10 10 rect
180 180 10 10 rect
110 145 10 10 rect
180 145 10 10 rect

;

: init 
randomize
;




: run
cls

-40 ystart ! 
 demo6 
; 


init
page
\ type run to test 
