
\ DICE GENERATOR for UF Forth
\ \\\\\\\\\\\\\\\\\\\\\\\\

\ SRC: https://gitlab.com/garvalf/forth-is-fun

\ see "pig.f" for a more complete example


: info 
page
." type 'ascii' or 'visual' for the rendering mode "
." then type 'run' to see the demo (clic or type 'd' to roll) "
;



\ hour second + seed  \ to make rnd more random
randomize

variable mynumber
variable visual

1 visual !

256 buffer: nimg

\ s" lines.f" included 
\ include lines.f
\ include rectangles.f


: CR cr ;

: SPACE space ;

: :NONAME :noname ;



: dice
page
\ 120 120 position
12 12 position
nimg spritedata
h# f6 auto
8 0 do
nimg 128 fileread
nimg spritedata
h# f6 auto
h# 1 sprite
drop
loop
page
;                     

\ this part will load external graphics

: dice01
s" media/dice01-10x08.icn" filename
dice
;

: dice02
s" media/dice02-10x08.icn" filename
dice
;

: dice03
s" media/dice03-10x08.icn" filename
dice
;

: dice04
s" media/dice04-10x08.icn" filename
dice
;

: dice05
s" media/dice05-10x08.icn" filename
dice
;


: dice06
s" media/dice06-10x08.icn" filename
dice
;


: Day 
h# f0f7 
h# f06d 
h# f02b 
colors  
;       

: Night                 
h# 0ff7 
h# 0f6d 
h# 0f2b 
colors 
;



: rndmax0 rnd 2dup over < if drop 
swap drop exit then 2drop recurse ;

: rndmax rnd abs 10 mod  
2dup over 
< if drop swap drop exit then 2drop recurse ;

: t rnd abs 10 mod . ; 


: convert_d6v1
page
20 6 at-xy cr
mynumber @ 1 = if  
     ."     .-----.    " CR 
     ."     |     |    " CR 
     ."     |  o  |    " CR
     ."     |     |    " CR
     ."      -----     " space CR else
mynumber @ 2 = if 
     ."     .-----.    " CR 
     ."     |    o|    " CR 
     ."     |     |    " CR
     ."     |o    |    " CR
     ."      -----     " space CR else
mynumber @ 3 = if  
     ."     .-----.    " CR 
     ."     |    o|    " CR 
     ."     |  o  |    " CR
     ."     |o    |    " CR
     ."      -----     " space CR else
mynumber @ 4 = if 
     ."     .-----.    " CR 
     ."     |o   o|    " CR 
     ."     |     |    " CR
     ."     |o   o|    " CR
     ."      -----     " space CR else
mynumber @ 5 = if  
     ."     .-----.    " CR 
     ."     |o   o|    " CR 
     ."     |  o  |    " CR
     ."     |o   o|    " CR
     ."      -----     " space CR else
mynumber @ 6 = if  
     ."     .-----.    " CR 
     ."     |o   o|    " CR 
     ."     |o   o|    " CR
     ."     |o   o|    " CR
     ."      -----     " space CR 
then then then then then then
;


: convert_d6v3
mynumber @ 1 = if dice01 else
mynumber @ 2 = if dice02 else
mynumber @ 3 = if dice03 else
mynumber @ 4 = if dice04 else
mynumber @ 5 = if dice05 else
mynumber @ 6 = if dice06  
then then then then then then 
;

: d6-temp 
depth
0 > if 
    1- dup 0 > if recurse 
	then
then
\ 6 rndmax 
rnd abs 6 mod 
\ rnd
1 +
\ dup convert_d6
\ dup 
\ .
mynumber ! 
\ mynumber @ .

convert_d6v3
space  
;

: d6
rnd abs 6 mod 
1 +
mynumber ! 
visual @ 1 = if
     convert_d6v3
else
     convert_d6v1
then
space  
;


: d6asc 
depth
0 > if 
    1- dup 0 > if recurse 
	then
then
rnd abs 6 mod 
1 +
mynumber ! 
convert_d6v1
space  
cr
;


: d d6 ; 


\ Day

page



\ simple clic test

: controllerloop

\ If the escape key is pressed
jkey 27 =  if
\ We reset the tree vectors so UF can take over
\ It is also possible to just send a 0 to each vector. I feel   \ is less confusing to send an xt.
	\  page screensize@ fill-mode NW 1 color paint
	['] noop svector
	['] noop jvector
	['] noop mvector
  else
\ if d is pressed
     jkey 100 =  if
          d6
     else 
     brk
     then
     brk
  then
;

: mouseloop3
mstate 1 = if
 d6
  then
wait ;
  
  
: demo
page
['] mouseloop3 mvector
['] controllerloop jvector
['] abort  wait 
;
  

: ascii 0 visual ! ;
: graphic 1 visual ! ;



defer run


:noname demo ; is run

\ run
\ save dice.rom

\ bye

page 

\ type 'info' or 'run'
