
\ GAME OF PIG for UF Forth
\ \\\\\\\\\\\\\\\\\\\\\\\\

\ SRC: https://gitlab.com/garvalf/forth-is-fun

\ http://cs.gettysburg.edu/projects/pig/piggame.html

\ start like this to create a standalone .rom file:
\ uxncli ufx.rom < pig.f (doesn't work yet)
\ uxnemu pig.rom

' noop is prompt

: info 
page
." type 'run' to play (WIP game) "
;

\ hour second + seed  \ to make rnd more random
randomize

variable mynumber

256 buffer: nimg

\ s" lines.f" included 
include lines.f
include rectangles.f
include media/dice01-10x08.f
include media/dice02-10x08.f
include media/dice03-10x08.f
include media/dice04-10x08.f
include media/dice05-10x08.f
include media/dice06-10x08.f
include media/pig08x08.f





: dice-sp
12 12   position
h# f6 auto
8 0 do
h# 1 sprite
loop
0 auto
page
;

: dice01 ( x y -- )
dice01-10x08 spritedata
dice-sp
; 

: dice02 ( x y -- ) dice02-10x08 spritedata dice-sp ; 

: dice03 ( x y -- ) dice03-10x08 spritedata dice-sp ; 

: dice04 ( x y -- ) dice04-10x08 spritedata dice-sp ; 

: dice05 ( x y -- ) dice05-10x08 spritedata dice-sp ; 

: dice06 ( x y -- ) dice06-10x08 spritedata dice-sp ; 

: Day 
h# f0f7 
h# f06d 
h# f02b 
colors  
;       

: Night                 
h# 0ff7 
h# 0f6d 
h# 0f2b 
colors 
;


: Amber                 
h# 3f0d   
h# 9c08 
h# 0213 
colors 
;

: Green
h# df07 
h# 8c0f 
h# 3214 
colors 
;

: Theme-pumpin
h# f7f0 
h# 6df0 
h# 2bf0 
colors 
;


: rndmax rnd abs 10 mod  
2dup over 
< if drop swap drop exit then 2drop recurse ;

: t rnd abs 10 mod . ; 





: convert_d6v3
mynumber @ 1 = if dice01 else
mynumber @ 2 = if dice02 else
mynumber @ 3 = if dice03 else
mynumber @ 4 = if dice04 else
mynumber @ 5 = if dice05 else
mynumber @ 6 = if dice06  
then then then then then then 
;



page

\ save dice.rom

\ bye


\ Pig game 
\ \\\\\\\\\

variable score
variable computer-score
variable computer-loose
variable turn-score
variable turn
variable winner
variable lastsecond \ for timer

100 constant MAXSCORE

0 score !
0 computer-score !
0 computer-loose !
0 turn-score !
0 turn !  \ 0 = player / 1 = computer
2 winner !  \ 0 = player / 1 = computer / 2 = no one

\ defers

defer pass-turn
defer Display-winner
defer Display-init
defer Display-infos
defer Display-interface
defer Display-score
defer on-mouse-pig


\ call buttons coordinates

: button01 ( x0 y0 x1 y1 )
20 81 74 105
;

: button02
80 81 134 105
;

: disp nb @ (.) ctype 10 cout ;  

: debugString  postpone ctype 10 cout ; immediate
: debugVar   postpone (.) ctype 10 cout ; immediate 
: debugTime hour minute second (.) (.) (.) debugString ;  


: test-win-state
 s" it is time " debugString
   hour (.) debugString s" :" debugString
   minute (.) debugString s" :" debugString
   second (.) debugString cr \ debug
  \ DebugTime
 s" test win state... " ctype 10 cout \ debug
 0 turn @ =  if   \ if player
    score @ mynumber @ + MAXSCORE >= if 
    s" Player won!!!! " ctype 10 cout \ debug
    0 winner !
    score @ mynumber @ +
    \ Display-winner \ display error
    exit
    then
  else
    computer-score @ mynumber @ + MAXSCORE >= if 
    s" Computer won!!!!! " ctype 10 cout \ debug
    1 winner !
    computer-score @ mynumber @ +
    exit
then then
;

: d6
rnd abs 6 mod 
1 +
mynumber ! 
convert_d6v3
space  


;




: waitnow ( sec -- )

second lastsecond !
begin ( do nothing ) ( sec ) dup
 \    s" waiting... " ctype 10 cout 
 \    lastsecond @ (.) ctype 10 cout  \ debug
 \    second (.) ctype 10 cout  \ debug
second swap ( sec )  - lastsecond @ > until drop
; 

: computer-play-d6
d6

Display-init
Display-infos
\ Display-interface
Display-score
Display-infos

s" computer made " ctype 10 cout \ debug
mynumber @ (.) ctype 10 cout  \ debug



test-win-state

mynumber @ 1 > if 
turn-score @ mynumber @ + turn-score !
else
0 mynumber ! 0 turn-score ! 1 computer-loose ! pass-turn
then

;

: computer-turn
1 turn !
0 turn-score !
0 mynumber !
0 computer-loose !
\ 0 waitnow
computer-play-d6

3 1 do 
     computer-loose @ 1 = if
     0 mynumber !
     exit
     \ do nothing
     else 
    \ 1 waitnow
     Display-infos
     computer-play-d6 
     then
loop 

pass-turn
;


: play-d6 

1 turn @ = if \ if computer's turn
     computer-turn
     exit
else
d6
mynumber @ 1 = if  
     0 turn-score !
     s" Player made a 1, too bad! " ctype 10 cout \ debug
     pass-turn
 else
 test-win-state
 s" player made " ctype 10 cout \ debug
 mynumber @ (.) ctype 10 cout  \ debug
    turn-score @ mynumber @ + turn-score !
    
then then 
;


:noname  \ : pass-turn

0 turn @ = if \ if player's turn
s" player passes turn  " ctype 10 cout \ debug
     score @ turn-score @ + score !
     0 turn-score !
     1 turn !
     exit
     s" exit test " ctype 10 cout
else
\ 1 turn @ = if \ if computer's turn
s" computer passes turn " ctype 10 cout \ debug
     computer-score @ turn-score @ + computer-score !
     0 turn-score !
     0 turn !
then 
; is pass-turn




: pig ( x y -- )

pig08x08 spritedata
  position
h# 76 auto
8 0 do
h# 41 sprite
loop
0 auto
; 


:noname \ Display-init
	screensize@ 0 - fill-mode fg NW 1 color paint
	cursor spritedata
    mouse position pixel-mode fg 3 color sprite
; is  Display-init

:noname \ Display-interface 
    140 54 pig
    \ display buttons
    button01 button
; is  Display-interface 

:noname \  Display-score
    18 16 at-xy score @ .
    18 18 at-xy computer-score @ .
    cr
; is Display-score

:noname  \ Display-winner
winner @ 2 < if 
     winner @ 0 = if 
          18 4 at-xy s" Player won!!" type
     else 
          18 4 at-xy s" Computer won!!" type
     then 
     Display-score
    then 
; is Display-winner


:noname \ Display-infos
    1 1 at-xy s" Pig Game" type
    15 1 at-xy mynumber @ . 
    4 11 at-xy s" play" type
    0 turn @ = if 11 11 at-xy s" hold" type then
    2 14 at-xy s" Turn score:" type
    15 14 at-xy turn-score @ .
    2 16 at-xy s" Your score:" type
    2 18 at-xy s" Computer score:" type
    Display-score
    
    
    turn @ 0 = if 
          18 2 at-xy s" Player's turn" type
     else 
          18 2 at-xy s" Computer turn" type
     then
    
    Display-winner 
; is  Display-infos


variable 'button
variable 'run

: BUTTON 'button @ execute ;

: RUN 'run @ execute ;

: MyButton  ( button xt -- )

mouse

BUTTON \ fetch 'button value
 \ => 'button @ execute

\ test if mouse clic 
\ is within the button's boundaries
 4 pick
  > if  \ test y 
 4 pick 
 > if  \ test x 
 
 2 pick  \ test y 
 < if  \ test x
 rot
 < if
  drop
 RUN   \ fetch 'run xt action value
 \ => 'run @ execute
  else else  else   else 
  \ TODO add more drop to clear stack
  then  then 
  \ drop 
  \ for clic on left but crashes if first clic on button
  then  then 
  \ 0   .s (.) ctype 10 cout  \ debug
;



:noname \ on-mouse-pig

    Display-init
    Display-infos
    Display-interface
    
mstate 1 = if

    ['] button01 'button !  \ store button01 pointer
    ['] play-d6 'run !      \ store action pointer
    MyButton                \ execute 
    
    turn @ 1 = if \ if computer's turn
        \ do nothing
    else    
        ['] button02 'button !
        ['] pass-turn 'run !
        MyButton
   then
   
  then
brk
; is  on-mouse-pig 


: on-controller-pig

\ If the escape key is pressed
jkey 27 =  if
\ We reset the tree vectors so UF can take over
\ It is also possible to just send a 0 to each vector. I feel   
\ is less confusing to send an xt.
	\ page screensize@ fill-mode NW 1 color paint
	['] noop svector
	['] noop jvector
	['] noop mvector
  else
\ if d is pressed
     jkey 100 =  if
          d6
     else 
     brk
     then
     brk
  then
;



: Input-test 
	['] on-mouse-pig mvector
    ['] on-controller-pig jvector
    ['] brk svector
    brk
;

: Roll-dice 

; 

: Test-player

; 


: Test-scoring

; 

: Computer-playing-decision

;


: pigapp
\ initialise variables
    0 score !
    0 computer-score !
\ clear screen
    cls page
 
\ display info 
    1 1 at-xy s" left-clic to play" type
    Display-interface
\ 1 38 at-xy s" esc to exit" type
\ test if the player is clicking on screen
\      or typing on keyboard
    Input-test 
; 


\ end pig game 
\ \\\\\\\\\\\\\

 
 
defer run



\ type "demo" to be able to click to roll the dice


\ :noname diceapp ; is run

 :noname pigapp ; is run


: boot 
cls  
\ pigapp 
;

\ true [if]
: boo0
h# 0b75 h# 0da6 h# 0db8 colors
\ Day 
\ run bye ;
run ;


\ save pig.rom
\ bye
\ [then]

\ .( enter "run" to start ... )

\ run 

\ save pig.rom
\ bye
\ can't make it save the rom and make it boot


\ page
\ type "run" to start the game
