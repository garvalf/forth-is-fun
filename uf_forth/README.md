

# UF Forth

## How to use UF Forth

Get the UXN emulator:

- https://git.sr.ht/~rabbits/uxn/

Compile it or get the binaries.

UF Forth can be found there: 

- http://www.call-with-current-continuation.org/uf/uf.html


For convenience the UF roms are included with this archive.

Run it with:

``uxnemu uf8/ufx.rom < someforthcode.f``

There is also a block editor, which is a great tool for live coding,
loading external files and experimentation.


Quickstart:

```
make live
1 load
1 edit
right-clic on "Doc"
right-clic on "Code"
right-clic on Lines
right-clic on Rectangles
```


## More files

- [Bresenham's line algorithm](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/uf_forth/lines.f)
- [Display Sprites](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/uf_forth/sprites.f)
- [Pig Game (w.i.p.)](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/uf_forth/pig.f)
- [Project folder on Github](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/uf_forth/)


## Tips 

Some tools to work with UF:

- convert blocks: use https://github.com/ablevm/forth-scr.git

- convert binary (sprites) to ascii: use xxd: ``xxd -p -g 2 -c 8 $1 | sed 's/../& /g'`` (see media/convert_bin.sh)

you can convert images, icons and sprites using xxd, just export a .png file, convert it with .tga (with ``convert``) and load it into noodle.rom (https://git.sr.ht/~rabbits/noodle), it will export it to .icn. Convert .icn with xxd and include it into .f code...


