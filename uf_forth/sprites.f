
\ SPRITE Examples for UF Forth
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\

\ SRC: https://gitlab.com/garvalf/forth-is-fun

\ with help from 
\ https://sr.ht/~maleza/enjoying-forth/

\ allocate memory for buffer


: info ." Sprite loading examples " ; 


256 buffer: nimg

\ we can load external files
\ made with UXN's noddle.rom
\ or some .tga imported into noddle.rom

\ beware if you save to a .rom it won't embed the 
\ external files


: pig
s" media/pig08x08.icn" filename
190 65 position
nimg spritedata
h# 38 auto  \ I don't understand how this value is found
8 0 do
nimg 64 fileread
nimg spritedata
h# f6 auto
h# 1 sprite
drop
loop
h# 0 auto
; 

: dragonswap08
s" media/swap08x08.icn" filename
260 40 position
nimg spritedata
h# 38 auto  \ I don't understand how this value is found
8 0 do
nimg 64 fileread
nimg spritedata
h# f6 auto
h# 1 sprite
drop
loop
h# 0 auto
; 

: dragonswap
s" media/swap10x10.icn" filename
270 40 position
nimg spritedata
h# 38 auto  \ I don't understand how this value is found
h# 10 0 do
nimg 128 0 + fileread
nimg spritedata
h# f6 auto
h# 4 sprite  \ quality of the sprite: color, transp, mirror... 
drop
loop
h# 0 auto
; 

: dragonswap0  \ doesn't work (too big)
s" media/swap1cx1c.icn" filename
260 40 position
nimg spritedata
h# f6 auto  \ I don't understand how this value is found
h# 1c 0 do  \ 0b ?
nimg 299 fileread
nimg spritedata
h# f6 auto
h# 1 sprite
drop
loop
h# 0 auto
; 





: rino  ( x y -- ) 
s" media/rino10x0b.icn" filename
120 140 position
h# 0b 0 do
     nimg 128 fileread drop
     nimg spritedata
     h# f6 auto
     h# 02 sprite
loop
 h# 0 auto
;



: dice
page
\ 120 120 position
12 12 position
nimg spritedata
h# f6 auto
8 0 do
nimg 128 fileread
nimg spritedata
h# f6 auto
h# 1 sprite
drop
loop
page
;  


: dice01
s" media/dice01-10x08.icn" filename
dice
;

\ You can also convert your icn files to code with xxd
\ and embed them into .f code
\ Look at media/convert_bin.sh 

create garvs hex
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 20 c, 30 c, 28 c, 2c c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 30 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 3f c, 
26 c, 43 c, 41 c, 40 c, 40 c, 80 c, 80 c, f8 c, 
00 c, 00 c, 80 c, c0 c, e0 c, 73 c, 3c c, 00 c, 
00 c, 03 c, 0c c, 30 c, c0 c, 00 c, 00 c, 00 c, 
d0 c, 10 c, 08 c, 08 c, 08 c, 08 c, 08 c, 08 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 3f c, 20 c, 40 c, 
00 c, 01 c, 01 c, 01 c, 0f c, ff c, ff c, 00 c, 
ff c, 01 c, 00 c, 00 c, 00 c, f8 c, ff c, 00 c, 
ff c, ff c, 07 c, 00 c, 00 c, 00 c, c0 c, 00 c, 
fe c, ff c, ff c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, f0 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
04 c, 04 c, 04 c, 04 c, 04 c, 04 c, 02 c, 02 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
40 c, 40 c, 40 c, 80 c, 80 c, 60 c, 30 c, 1c c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
02 c, 02 c, 01 c, 01 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 80 c, 80 c, 40 c, 20 c, 
0e c, 07 c, 01 c, 00 c, 00 c, 00 c, 00 c, 3f c, 
00 c, 80 c, c0 c, f0 c, 7c c, 3e c, 0f c, ff c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 80 c, c0 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
20 c, 10 c, 10 c, 08 c, 04 c, 04 c, 02 c, 02 c, 
41 c, 40 c, 20 c, 10 c, 08 c, 07 c, 00 c, 00 c, 
ff c, 00 c, 00 c, 00 c, 00 c, f0 c, 0f c, 00 c, 
f0 c, f8 c, 00 c, 00 c, 00 c, 00 c, ff c, 03 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, c0 c, ff c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 80 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 01 c, 
01 c, 05 c, 02 c, 14 c, 08 c, 50 c, 20 c, 40 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
01 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
ff c, ff c, ff c, 7f c, 3f c, 1f c, 07 c, 00 c, 
e0 c, f8 c, fc c, fc c, fe c, ff c, ff c, 7f c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 80 c, 
00 c, 01 c, 02 c, 04 c, 08 c, 10 c, 20 c, 40 c, 
80 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
07 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
c0 c, 71 c, 0e c, 00 c, 00 c, 00 c, 00 c, 00 c, 
80 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 
00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 00 c, 

decimal

: garvalf

garvs spritedata
126 36   position
h# 76 auto
8 0 do
\ h# 08 auto
\ h# f6 auto
h# 41 sprite
\ drop
loop
 0 auto
; 


create merveilless hex

00 c, 7e c, 7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 
00 c, 00 c, c0 c, f0 c, f8 c, fc c, ff c, ff c, 
00 c, 07 c, 07 c, 07 c, 07 c, 07 c, 07 c, 87 c, 
00 c, f0 c, fc c, ff c, ff c, ff c, ff c, ff c, 
00 c, 0f c, 3f c, ff c, ff c, ff c, ff c, ff c, 
00 c, e0 c, e0 c, e0 c, e0 c, e0 c, e0 c, e1 c, 
00 c, 00 c, 03 c, 0f c, 1f c, 3f c, ff c, ff c, 
00 c, 7e c, fe c, fe c, fe c, fe c, fe c, fe c, 
7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
87 c, c7 c, e7 c, e7 c, f7 c, f7 c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
e1 c, e3 c, e7 c, e7 c, ef c, ef c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
fe c, fe c, fe c, fe c, fe c, fe c, fe c, fe c, 
7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 03 c, 07 c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, 00 c, c0 c, 
ff c, ff c, ff c, ff c, ff c, ff c, 1f c, 7f c, 
ff c, ff c, ff c, ff c, ff c, ff c, f8 c, fe c, 
ff c, ff c, ff c, ff c, ff c, ff c, 00 c, 03 c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
fe c, fe c, fe c, fe c, fe c, fe c, c0 c, e0 c, 
0f c, 1f c, 3f c, 3f c, 7f c, 7f c, 7f c, 7f c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
e0 c, f1 c, f9 c, fb c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
07 c, 8f c, 9f c, df c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
f0 c, f8 c, fc c, fc c, fe c, fe c, fe c, fe c, 
7f c, 7f c, 7f c, 7f c, 3f c, 3f c, 1f c, 0f c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, fb c, f9 c, f0 c, e0 c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, df c, 9f c, 0f c, 07 c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
fe c, fe c, fe c, fe c, fc c, fc c, f8 c, f0 c, 
07 c, 03 c, 7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
c0 c, 00 c, ff c, ff c, ff c, ff c, ff c, ff c, 
3f c, 1f c, ff c, ff c, ff c, ff c, ff c, ff c, 
fc c, f8 c, ff c, ff c, ff c, ff c, ff c, ff c, 
03 c, 00 c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
e0 c, 80 c, fe c, fe c, fe c, fe c, fe c, fe c, 
7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, f7 c, f7 c, e7 c, e7 c, c7 c, 87 c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
ff c, ff c, ef c, ef c, e7 c, e7 c, e3 c, e1 c, 
ff c, ff c, ff c, ff c, ff c, ff c, ff c, ff c, 
fe c, fe c, fe c, fe c, fe c, fe c, fe c, fe c, 
7f c, 7f c, 7f c, 7f c, 7f c, 7f c, 7e c, 00 c, 
ff c, ff c, fc c, f8 c, f0 c, c0 c, 00 c, 00 c, 
87 c, 07 c, 07 c, 07 c, 07 c, 07 c, 07 c, 00 c, 
ff c, ff c, ff c, ff c, ff c, fc c, f0 c, 00 c, 
ff c, ff c, ff c, ff c, ff c, 3f c, 0f c, 00 c, 
e1 c, e0 c, e0 c, e0 c, e0 c, e0 c, e0 c, 00 c, 
ff c, ff c, 3f c, 1f c, 0f c, 03 c, 00 c, 00 c, 
fe c, fe c, fe c, fe c, fe c, fe c, 7e c, 00 c, 

decimal


: merv ( x y -- )

merveilless spritedata
 position
h# 76 auto
8 0 do
\ h# 08 auto
\ h# f6 auto
h# 41 sprite
\ drop
loop
 0 auto
; 


: merveilles
308 230 merv
;

: run
rino
dice01
merveilles
garvalf
pig
dragonswap
;

page

\ type "run" to load the sprites
run
