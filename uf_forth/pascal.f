( Pascal's Triangle - single precission )
\ not working in uf forth, but works with gforth

: fact  ( n -- n )
( calculates the factorial of n )
dup 1 > if
	dup 1- recurse *
else
	drop 1
then ;


: binomial ( n n -- n )
( Calculates the binomial coefficient of n over m
\  where n is the zero-indexed row and m is the zero-indexed
\  column of a pascal's triangle )

2dup
fact 
swap
fact 
2swap
-
fact 
rot
* / 
;


: pascal ( n -- )
( draws the pascal's triangle up to row no. n )

	cr
	1 + 0 do
		i 1 + 0 do
			j i binomial . ." "
		loop
		cr
	loop ;


12 pascal
