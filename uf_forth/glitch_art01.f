
\ Random Pixel art Examples for UF Forth
\ and sprite generation 
\ (second way of doing it)
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\


\ SRC: https://gitlab.com/garvalf/forth-is-fun

hour second + seed  \ to make rnd more random
randomize

: rndmax rnd abs 10 mod  
2dup over 
< if drop swap drop exit then 2drop recurse ;



: pix0 ( a b -- )
 130 + swap 130 + swap
 position
 00 pixel
;

: pix1 ( a b -- )
 \ 130 + swap 130 + swap
 mouse rot + rot rot + swap
 position
 01 pixel
;

: pix1b ( a b -- )
 \ 130 + swap 130 + swap
 mouse rot + rot rot + swap
 position
 42 pixel
;

variable x1
variable y1

: pix2 ( a b -- )
 \ mouse rot + rot rot + swap
 x1 @ y1 @ rot + rot rot + swap
 position
 01 pixel
;

: pix3 ( a b -- )
 \ mouse rot + rot rot + swap
 x1 @ y1 @ rot + rot rot + swap
 position
 01 pixel
;

: pix42 ( a b -- )
 \ mouse rot + rot rot + swap
 x1 @ y1 @ rot + rot rot + swap
 position
 42 pixel
;


: pix43 ( a b -- )
 \ mouse rot + rot rot + swap
 x1 @ y1 @ rot + rot rot + swap
 position
 43 pixel
;





: random_sprite8
32 0 do
rnd abs 64 mod 
  8 /mod  pix2
loop
;

: random_sprite16
90 0 do
rnd abs 256 mod 
  16 /mod  
   rnd abs 10 mod 6 < if
    pix42 else pix3 then
loop
;

: paint
256 0 do
i
  16 /mod  pix1
loop
;




: filler2   \ small carré
64 0 do
64 0 do
 i x1 ! j y1 !
 random_sprite16
 10 +loop
 10 +loop
;

: filler3  \ snow
256 0 do
256 0 do
 i x1 ! j y1 !
 random_sprite16
 10 +loop
 10 +loop
;








\ \ \\\\\
\ from https://sr.ht/~maleza/enjoying-forth/



: paint ( x y n-- ) -rot position pixel ;

0 constant pixel-mode 

128 constant fill-mode

: bg noop ;

: fg 64 or ;

: flip-x 32 or ;

: flip-y 16 or ;

: NW flip-x flip-y ; 

: NE flip-y ; 

: SW flip-x ;

: SE noop ;

: color 1 - + ;

: clean-bg 0 0 [ fill-mode 1 color literal ] paint ;


: cls clean-bg ;




: on-controller-lines

 jkey 120 = if \ x or space
   page screensize@ fill-mode NW 1 color paint
   filler3
 else
 
 \ If the escape key is pressed
jkey 27 =  if
\ We resset the tree vectors so UF can take over
\ It is also possible to just send a 0 to each vector. I feel   \ is less confusing to send an xt.
	page screensize@ fill-mode NW 1 color paint
	['] noop svector
	['] noop jvector
	['] noop mvector

 else
	 brk
  then
;

: app
    ['] on-controller-lines jvector
    ['] brk svector
    page
    filler3
    1 39 at-xy s" x to redraw, esc to exit" type
    brk
;


page
\ run
app 
\ filler2

: run
app
 \ display_sprite
 \ random_sprite16
;

