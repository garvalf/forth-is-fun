
\ Implementation of Bresenham's algorithm for drawing lines
\ for UF Forth 

\ https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm 
\ https://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm

\ SRC: https://gitlab.com/garvalf/forth-is-fun
\ some code taken from https://sr.ht/~maleza/enjoying-forth/


variable x
variable y
variable x0
variable y0
variable x1
variable y1
variable deltax
variable deltay
variable delta
variable yi
variable xi


variable pixcolor
1 pixcolor !

\ background 

: pixb0 ( a b -- )
 position
 00 pixel
;

: pixb1 ( a b -- )
 position
 01 pixel
;

: pixb2 ( a b -- )
 position
 02 pixel
;

: pixb3 ( a b -- )
 position
 03 pixel
;

: pix0 ( a b -- )
 position 
 40 pixel 
;

\ foreground 
: pix1 ( a b -- )
 position 
 41 pixel 
;

: pix2 ( a b -- )
 position 
 42 pixel 
;

: pix3 ( a b -- )
 position 
 43 pixel 
;

\ choose one of the 3 varvara's color:

: pix pixb1 ;
 
 
\ Bresenham's algorithm
\ \\\\\\\\\\\\\\\\\\\\\

: lineLow ( x0 y0 x1 y1 )
\ storing data from the stack
\  cover slopes between 0 and -1 by checking 
\ whether y needs to increase or decrease (i.e. dy < 0) 
y1 ! x1 ! y0 ! x0 !  
x1 @ x0 @ - deltax !
y1 @ y0 @ - deltay !
1 yi !

deltay @ 0 <= if
-1 yi !
\ we add one to invert otherwise five invert will be minus 6
deltay @ invert 1 + deltay !  
then

deltay @ 2 * deltax @ - delta !
y0 @ y !
x0 @ x !

x1 @ x0 @ do                    \  ( loop )
\ ." x= " x ? ." y= " y ? ." delta= " delta ? cr  ( debug ) 

pixcolor @ 1 = if
 x @ y @ pixb1 else
pixcolor @ 2 = if
 x @ y @ pixb2 else
pixcolor @ 3 = if
 x @ y @ pixb3 else
then then then

delta @ 0 >= if
 y @ yi @ + y !
 delta @ deltay @ deltax @ - 2 * + delta !
 else
 delta @ deltay @ 2 * + delta !
then
 x @ 1 + x !
loop 
;


: lineHigh ( x0 y0 x1 y1 )
\ switching the x and y axis, 
\ for positive or negative steep slopes 
y1 ! x1 ! y0 ! x0 !  
x1 @ x0 @ - deltax !
y1 @ y0 @ - deltay !
1 xi !

deltax @ 0 <= if
-1 xi !
\ we add one to invert otherwise five invert will be minus 6
deltax @ invert 1 + deltax !  
then

deltax @ 2 * deltay @ - delta !
y0 @ y !
x0 @ x !

y1 @ y0 @ do                    \  ( for loop )
\ ." x= " x ? ." y= " y ? ." delta= " delta ? cr  ( debug ) 

pixcolor @ 1 = if
 x @ y @ pixb1 else
pixcolor @ 2 = if
 x @ y @ pixb2 else
pixcolor @ 3 = if
 x @ y @ pixb3 else
then then then



delta @ 0 >= if
 x @ xi @ + x !
 delta @ deltax @ deltay @ - 2 * + delta !
 else
 delta @ deltax @ 2 * + delta !
then
 y @ 1 + y !
loop 
;

: line  ( x0 y0 x1 y1 )
y1 ! x1 ! y0 ! x0 ! 
y1 @ y0 @ - abs 
x1 @ x0 @ - abs
<= if 
 x0 @ x1 @ >= if
   x1 @ y1 @ x0 @ y0 @ lineLow
 else 
   x0 @ y0 @ x1 @ y1 @ lineLow
 then
else
 y0 @ y1 @ >= if
  x1 @ y1 @ x0 @ y0 @ lineHigh
 then 
  x0 @ y0 @ x1 @ y1 @ lineHigh
then 
;


\ end of Bresenham's algorithm
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\

512 constant SCREENX 
320 constant SCREENY


\ moveto & lineto for plotting with only 2 coordinates
\ lineto will keep tracks of the latest point

variable xt0
variable yt0
variable xt1
variable yt1

\ store the first point's coordinates

: moveto ( x1 y1 )
 yt0 ! xt0 ! 
;

\ store the second point's coordinates
\ replace previous point with latest

: lineto ( x2 y2 )
  yt1 ! xt1 ! 
  xt0 @ yt0 @ xt1 @ yt1 @ line
  xt1 @ yt1 @ moveto
;

: lt lineto ;

: uf
\ 0 320 moveto
\ U
2 pixcolor !
15 240 moveto 15 275 lineto 20 280 lineto 35 280 lineto 
40 275 lineto 40 240 lineto 35 250 lineto 35 270 lt 
30 275 lt 25 275 lt 20 270 lt 20 250 lt 15 240 lt
\ F
50 240 moveto 50 280 lt 55 270 lt 55 255 lt 60 255 lt 
60 250 lt 55 250 lt 55 245 lt 75 240 lt 50 240 lt
1 pixcolor !
;


: randomlines ( x -- )
\ 0 SCREENY moveto
 rnd abs SCREENX mod 
 rnd abs SCREENY mod 
 moveto
 1 do 
  rnd abs SCREENX mod 
  rnd abs SCREENY mod 
 lineto
 loop 
;


\ clear screen

: cls
page
512 0 do 
320 0 do
j i  pixb0
loop loop
;

\ clear background 

: clsf
page
512 0 do 
320 0 do
j i  pix0
loop loop
;


 
\ test all directions



: diamond

25 50 50 25 line
50 25 75 50 line 
75 50 50 75 line  
50 75 25 50 line  
;

: diamond2

125 150 150 125 line
150 125 175 150 line 
175 150 150 175 line 
150 175 125 150 line  
;




: testline ( test all possible directions )

3 pixcolor !
SCREENX 2/ SCREENY 2/ 2dup 100 + swap 100 + swap line
SCREENX 2/ SCREENY 2/ 2dup 100 + swap 100 - swap line
SCREENX 2/ SCREENY 2/ 2dup 100 + swap 50 + swap line
SCREENX 2/ SCREENY 2/ 2dup 100 + swap 50 - swap line
SCREENX 2/ SCREENY 2/ 2dup 100 - swap 50 + swap line
SCREENX 2/ SCREENY 2/ 2dup 100 - swap 50 - swap line
SCREENX 2/ SCREENY 2/ 2dup 100 - swap 100 - swap line
SCREENX 2/ SCREENY 2/ 2dup 100 - swap 100 + swap line

SCREENX 2/ SCREENY 2/ 2dup 00 - swap 100 - swap line
SCREENX 2/ SCREENY 2/ 2dup 00 - swap 100 + swap line

SCREENX 2/ SCREENY 2/ 2dup 100 - swap 00 + swap line
SCREENX 2/ SCREENY 2/ 2dup 100 + swap 00 + swap line

SCREENX 2/ SCREENY 2/ 2dup 50 + swap 100 + swap line
SCREENX 2/ SCREENY 2/ 2dup 50 + swap 100 - swap line
SCREENX 2/ SCREENY 2/ 2dup 50 + swap 50 + swap  line
SCREENX 2/ SCREENY 2/ 2dup 50 + swap 50 - swap line
SCREENX 2/ SCREENY 2/ 2dup 50 - swap 50 + swap line
SCREENX 2/ SCREENY 2/ 2dup 50 - swap 50 - swap line
SCREENX 2/ SCREENY 2/ 2dup 50 - swap 100 - swap line
SCREENX 2/ SCREENY 2/ 2dup 50 - swap 100 + swap line
1 pixcolor !
;


\ a more complex drawing

: garvalf 
313 267 380 200 line
380 200 343 136 line
343 136 331 61 line
331 61 270 90 line
270 90 232 49 line
232 49 223 89 line
223 89 301 105 line
301 105 223 89 line
223 89 200 93 line
200 93 193 99 line
193 99 249 106 line
249 106 301 105 line
193 99 187 113 line
187 113 228 122 line
187 113 138 117 line
138 117 228 122 line 
138 117 128 147 line 
128 147 210 198 line
128 147 175 188 line
175 188 138 192 line
175 188 210 198 line  ( mouth )
138 192 210 198 line 
138 192 133 199 line
133 199 145 216 line
145 216 215 220 line
215 220 237 249 line
237 249 313 267 line
215 220 267 225 line
267 225 313 267 line
;



\ Demo

variable mousex
variable mousey

variable mousex-old
variable mousey-old 

138 mousex !
117 mousey !

: mouseloop2
  mouse mousey ! mousex !
  mstate 4 = if
 \ cls
  150 150 mousex @ mousey @ line
  else
  mstate 1 = if
  100 100 mousex @ mousey @ line
   \ mousex @ mousey @ pix
  then then wait ;



: mouseloop5

mouse mousey ! mousex !
 mousex @ mousey @ pix3
 
 mstate 1 = if
  1 pixcolor !
  mousex @ mousey @ lineto
   else
  mstate 4 = if 
 \  mousex @ mousey @ moveto
 2 pixcolor !
 mousex @ mousey @ lineto
 clsf
 then then wait ;


: demo ['] mouseloop5 mvector
  ['] abort jvector wait 
 ;
 



\ from "enjoying forth" 
\ \\\\\\\\\\\\\\\\\\\\\

: paint ( x y n-- ) -rot position pixel ;

0 constant pixel-mode 

128 constant fill-mode

: bg noop ;

: fg 64 or ;

: flip-x 32 or ;

: flip-y 16 or ;

: NW flip-x flip-y ; 

: NE flip-y ; 

: SW flip-x ;

: SE noop ;

: color 1 - + ;

: clean-bg 0 0 [ fill-mode 1 color literal ] paint ;

create cursor
hex 80 c, c0 c, e0 c, f0 c, f8 c, f8 c, e0 c, 10 c, decimal 


: on-mouse-lines
	screensize@ 8 - fill-mode fg NW 1 color paint
	cursor spritedata
    mouse position pixel-mode fg 2 color sprite
    mstate 1 = if
      mouse position
      pixel-mode bg 3 color pixel
      mouse moveto
    else 
    mstate 4 = if
      mouse 
      lineto
    then then 
    brk
;

: on-controller-lines

\ If the escape key is pressed
jkey 27 =  if
\ We resset the tree vectors so UF can take over
\ It is also possible to just send a 0 to each vector. I feel   \ is less confusing to send an xt.
	page screensize@ fill-mode NW 1 color paint
	['] noop svector
	['] noop jvector
	['] noop mvector
  else
	brk
  then
;

: app
	['] on-mouse-lines mvector
    ['] on-controller-lines jvector
    ['] brk svector
	1 38 at-xy s" left-clik to draw, right-clic for lines" type
    1 39 at-xy s" esc to exit" type
        
    brk
;

\ \\\\\\\\\\\\\\\\\\\\\\



\ init 

: run
page
testline
;

\ run
page

\ type run to see a test 
\ type app to draw
