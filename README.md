
# Forth is fun!

Some experiments with the FORTH language and system.

- WEB: https://garvalf.gitlab.io/forth-is-fun
- SRC: https://gitlab.com/garvalf/forth-is-fun/


![](swap.png)


The goal of this repository is to provide some working examples using various FORTH systems.


## ueforth

- https://github.com/flagxor/ueforth
- https://eforth.appspot.com/linux.html
- https://eforth.arduino-forth.com/web-eforth/index/ 


## WAForth / Thurtle

- https://github.com/remko/waforth
- https://mko.re/waforth/thurtle/


## UF Forth

- http://www.call-with-current-continuation.org/uf/uf.html
- see also https://sr.ht/~maleza/enjoying-forth/


## Gforth

- https://gforth.org/
- For graphical effects, install libtool-bin and libsdl2*dev. Tested on gForth 0.7.3 and 0.7.9 dev and SDL 2.0.5


## DX-FORTH

- for DOS. No official website, get it at: https://drive.google.com/drive/folders/1kh2WcPUc3hQpLcz7TQ-YQiowrozvxfGw


## Some other FORTH to explore...

- https://github.com/tabemann/zeptoforth Zeptoforth, for raspberry pico
- https://github.com/DSCFprog/PC32 a continuation of ColorForth
- ...


## Some useful tools

- https://github.com/ablevm/forth-scr   a command line tool that allows to convert Forth block files (*.blk) to ASCII text files and the way back.
