


# DXFORTH 

A modern Forth for DOS (latest version 4.55 is from 2023)


This repository only contains a striped down version of DX Forth. You can get the full distribution at the link provided below.

Run ``dosbox ./``. French users might need to type ``keyb fr`` for azerty keyboard support. Start DX.EXE from the DOS prompt.

## Quickstart

- ``include file``  \ load file.f is source code and make its words available
- ``using file``    \ load file.scr (in block format)
- ``list``          \ list current blocks
- ``0 edit``        \ edit block number 0



## 4THTOISE.F 

It is a turtle like application. Load it with ``include 4THTOISE``. Then type ``run`` or ``draw 10 forward 45 left 20 forward /draw`` for example, or ``draw`` and type the commands interactively.

- [4thtoise](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/dxforth/4THTOISE.F)


![](4thtoise01.png)


## More files

- [Project folder on Github](https://gitlab.com/garvalf/forth-is-fun/-/tree/main/dxforth)




## XPLGRAPH.SCR

XPLGRAPH.SCR is a library for displaying graphics in DX-FORTH. It is used by the previous projects.

```
XPLGRAPH.SCR

Graphics and sound functions

SETVID  ( mode -- )         set video mode
SETPAGE ( page -- )         set video page
CLEAR   ( -- )              clear screen (text & graphics)
PALETTE ( palette -- )      set color palette
SETPAL  ( addr -- )         set all 16 palette registers
MOVETO  ( x y -- )          move to coordinate XY
POINT   ( x y color -- )    plot pixel at XY
READPIX ( x y -- color )    read pixel at XY
LINE    ( x y color -- )    draw line to XY
XMAX    ( -- xmax )         max coordinate X (new)
YMAX    ( -- ymax )         max coordinate Y (new)
SOUNDX  ( mode ticks rate -- )  generate sound or delay
```


some nice palettes to try: 52 140 183 193 (+ setpal)


## Develop

you can recompile DX-FORTH using borland bcpp31

- see: https://archive.org/details/bcpp31

and also more about retro compilation: 

- https://fabiensanglard.net/Compile_Like_Its_1992/index.php
- https://github.com/jacmoe/retrogardens


## Links 

see also:

- http://dxforth.mirrors.minimaltype.com/   some tools made with dx forth (original Ed Smeda DX Forth home page)
- https://drive.google.com/drive/folders/1kh2WcPUc3hQpLcz7TQ-YQiowrozvxfGw lastest sources and binaries (full sources)
