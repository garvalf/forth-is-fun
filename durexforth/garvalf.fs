include gfx 

: roll ( x0 i*x u.i -- i*x x0 )
  dup 0= if drop exit then  swap >r 1- recurse r> swap
;

  
: myline ( 11 22 33 44 -- 33 44 11 22 ) 
3 roll 3 roll plot line
;


: garvalf 
243 220 310 153 myline
310 153 273 89 myline
273 89 261 14 myline
261 14 200 43 myline
200 43 162 2 myline
162 2 153 42 myline
153 42 231 58 myline
231 58 153 42 myline
153 42 130 46 myline
130 46 123 52 myline
123 52 179 59 myline
179 59 231 58 myline
123 52 117 66 myline
117 66 158 75 myline
117 66 68 70 myline
68 70 158 75 myline
68 70 58 100 myline
58 100 140 151 myline
58 100 105 141 myline
105 141 68 145 myline
105 141 140 151 myline
68 145 140 151 myline
68 145 63 152 myline
63 152 75 169 myline
75 169 145 173 myline
145 173 167 202 myline
167 202 243 220 myline
145 173 197 178 myline
197 178 243 220 myline
;

include mml

: torche
s" o4 d2<a2>f2c2d1f4e4e2d2<a2>f2c2d1f4e4d2d2<a2>f2e4d4d2<a4a4>f4f4e2a8d8f8e8c8d8<a4>f8c8e8d8c8d8a8f8e4<a2>d4f4f4e2f8e8d4<a2>d2<a2>f2e4d4d2<a4a4>f4f4e2a8d8f8e8c8d8<a4>f8c8e8d8c8d8a8f8<a4>f8e8<a8>c8c4<a4>d8f8e8d8a4<a4>f8e8<a8>c8c4c4d8f8e8c8e4<a4>f8e8<a8>c8c4<a4>d8f8e8d8a4<a4>f8e8<a8>c8c4c4d8f8e8c8e4d4<f4>d4<f4>c4<e4f4>c4<a2e2c8d4d8>c8<a4>d8"

s" o3 a1>c2c2d2a2d2e2<a1>c2c2d2a2d2d2a8d8f8e8c8d8<a8>d8f8c8e8d8c8d8a8f8d8g8a8f8<a8>c8e8d8<f2e2>f8g8d8c8a4f4<d4a4d4a4e4>f8e4f8a4<d4a4d4a4e4>f8e4f8a4a8d8f8e8c8d8<a8>d8f8c8e8d8c8d8a8f8d8e8a8f8<a8>c8e8d8d1f8g8d8c8a4f4<d4a4d4a4d4a4d4a4e4>f8e4f8<a4d4a4d4a4e4>f8e4e8c4<d4a4d4a4e4>f8e4f8<a4d4a4d4a4e4>f8e4e8c4<d4f8e8"

s" o2 d1<a4a4>e2d1f4e4e2d1<a4a4>e2d1f4e4d2d1<a4a4>e2e2f4g4a2f4d4d4d4c4c4d2a2<a4>d4d4e4d2a2<a4>d4d4e4d1<a4a4>e2e2f4g4a2f4d4d4d4c4c4d2a2d2a2<a4>d4d4e4d2a2<a4>d4d4e4d2a2<a4>d4d4e4d2a2<a4>d4d4e4f2d4f4c2<a4>c4e2c4<a4>c8d4.e2c8d4<a8>e8d4f8c8d4.e2c8d4.c2d2e2d1<a4a4>e2d1f4e4e2d1<a4a4>e2d1f4e4e2a2>d2"
play-mml
;

15 clrcol hires garvalf torche



