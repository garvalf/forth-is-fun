
\ --------------------------------------------
\                                           :;
\                  CYOA                     :;
\                                           :; 
\   Choose your own adventure in FORTH      :;
\                                           :;
\ https://gitlab.com/garvalf/forth-is-fun   :;
\                                           :; 
\ --------------------------------------------
\  
\ 
\ <!> This version is only a copy of the ueforth version
\     which is the main source for the CYOA

\ works with:
\   gforth 0.7.3->0.7.9 
\   ueforth v7.0.7.18 (cli & web)
\   DX FORTH 4.55
\   durexForth 4 


\ Select your Forth version (set it to 1):

0 CONSTANT UEFORTH
0 CONSTANT GFORTH
1 CONSTANT DUREXFORTH


: GFORTH?
GFORTH 1 = 
;

: DUREXFORTH?
DUREXFORTH 1 = 
;

: UEFORTH?
UEFORTH 1 = 
;


: init   \ initialise and check version
DUREXFORTH? IF
 s" include compat" EVALUATE \ for durexForth only
 s" : bye ; " EVALUATE
THEN
;

init

\ random nb generator

VARIABLE rnd   
HERE rnd !



\ for ueforth limitations

: MYUM* 
    UEFORTH?  IF 
       \ ." ueforth"
        1 */ ABS 0 SWAP 
    ELSE 
       \ ." gforth" 
       s" UM*" EVALUATE  \ can't call um* directly for ueforth
    THEN ;  \ doesn't work with gforth

\ end ueforth

: random  rnd @ 31421 *  6927 +  DUP rnd ! ;    \ random number (big)

: rnd10  ( u1 -- u2 ) random random MYUM* NIP 10 mod ;   
\ random number between 0 and 9

: rndn ( n_max -- u2 ) random random 1 + MYUM* NIP SWAP ( n ) mod ; 
\ random number between 0 and n

\ not used
: rnd>max ( n-max -- rnd-max ) rnd 2DUP OVER < IF 
        DROP SWAP DROP EXIT THEN 
    2DROP RECURSE  ;

: rnd>min ( n-min -- rnd-min ) rnd 2DUP OVER > IF 
        DROP SWAP DROP EXIT THEN 
    2DROP RECURSE  ;

VARIABLE answer 
VARIABLE demo


DEFER chap00   \ create words in advance so we can refer to them later
DEFER chap01
DEFER chap02
DEFER chap03
DEFER chap04
DEFER chap05
DEFER chap06
DEFER chap07
DEFER chap08
DEFER chap09
DEFER chap10


\ : compiled ." compiled!" CR ;  
: compiled ." "  ;  

: backtoforth exit ;  \ do nothing so leave

: chapini0 demo @ 1 = IF compiled EXIT THEN ;

: chapinit PAGE CR ."  ~[====> " CR ;

: theend CR ." ~ THE END ~" CR CR BYE ; 

\ this one doesn't work with ueforth:
 : ask0
\ PAD 3 ACCEPT DROP
\ 0 0 PAD 3 >NUMBER DROP DROP DROP  
\ \ convert pad to number and remove extra data in the stack
\ \  answer !   \ disable this one, use the stack
;

\ this one is quicker for the player, but doesn't work well with pforth
: ask
." > "
KEY 48 -  \ key leaves ascii code, which is 49 for 1, 50 for 2 and so forth...
\ \ answer !  \ disable this one, use the stack
;



: retry CR ." Not a valid answer, please retry. " 
PAD 10 0 FILL PAD 1 accept DROP answer @ .  ;
\ : retry ; 
\ we have to clear the pad before accepting new answer


VARIABLE ?gatekey
VARIABLE ?gateopen
VARIABLE ?bucketseen
VARIABLE ?dagger
VARIABLE ?visited00
VARIABLE ?dive01

0 ?gatekey !
0 ?bucketseen !
0 ?gateopen !
0 ?dagger !
0 ?visited00 !
0 ?dive01 !




:NONAME ( chap00: intro ) 
chapinit
?visited00 @ 0 = IF
CR
." @----------------------------------@ " CR
." / Welcome to the Dungeon of Forth! /" CR
." @~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-@" CR CR
." After a long travel which took several weeks from your homeland," CR
." you arrive in the garden adjacent to a tower. The lost tower of Forth!" CR 
key drop
."                       " CR 
."              ~        " CR 
."             /`        " CR 
."            / -`       " CR 
."           /-  -`      " CR 
."          /------`     " CR 
."          |/^` - |     " CR 
."          ||_|   |     " CR 
."          |  -   |     " CR 
."          |-   _ |     " CR 
."   _______|___|#||____ " CR 
CR
1 ?visited00 !
ELSE
." You are in the garden adjacent to the tower." CR
THEN
." Would you like to:" CR
." 1. explore to the left, near a pond?" CR
." 2. explore to the right, approaching the plants and flowers?" CR
." 3. go forth, in front of the portcullis?" CR
." 4. leave this place?" CR
ask 
\ answer @ 
CASE
1 OF chap01 ENDOF
2 OF chap04 ENDOF
3 OF chap05 ENDOF
4 OF chap07 ENDOF
0 OF backtoforth ENDOF
retry chap00 
ENDCASE
; IS chap00






:NONAME ( chap01: pond )
chapinit
6 rndn 
CASE
0 OF ." The pond is a very calm place." CR ENDOF
1 OF ." There is a frog in the border of the pond." CR ENDOF
2 OF ." This pond is quite dirty, there is mud preventing to see the bottom." 
    ?dive01 @ 0 = IF  \ do you want to dive now?
            1 ?dive01 ! THEN CR ENDOF
3 OF ." The wind is moving the herbs surrounding the pond. "  
    ?bucketseen @ 0 = IF  \ if bucket wasn't previously removed
         ." You discover a bucket among them. " 
        1 ?bucketseen ! THEN CR ENDOF
4 OF ." This is a tiny place, you no longer want to be there." CR ENDOF
." The pond is a calm place." CR ( default )
ENDCASE

." 1. Explore more this place" CR
." 2. Go back to the entrance" CR
?bucketseen @ 1 = IF 
    ." 3. Search the bucket" CR THEN
?dive01 @ 1 = IF 
    ." 4. Dive inside the pond" CR THEN
ask
\ answer @
CASE
1 OF chap01 ENDOF 
2 OF chap00 ENDOF 
3 OF ?bucketseen @ 1 = IF 
    3 ?bucketseen ! \ remove bucket from play
    chap08 THEN retry chap01 ENDOF
4 OF ?dive01 @ 1 = IF 
    3 ?dive01 ! \ remove it
    chap06 THEN retry chap01 ENDOF
retry chap01
ENDCASE 
; IS chap01


:NONAME ( chap02 )

; is chap02


:NONAME ( chap03: inside )
chapinit
." There are echoes from eerie sounds, but you tighten the handle "
." of the dagger and progress further." CR
." Who knows all the adventures you will experience in this exciting place?" 
CR
theend
; is chap03


:NONAME ( chap04: plants and flowers )
chapinit
." You walk to the right. "
." This garden is gorgeous, full of plants you can't even name." CR
?gatekey @ 0 = IF
." You find a key in the grass, which you keep with you." CR
1 ?gatekey !  \ now has key
THEN
." 1. Go back to the entrace of the garden." CR
ask
\ answer @
CASE
1 OF chap00 ENDOF
retry chap04
ENDCASE 
; is chap04


:NONAME ( chap05: portcullis )
chapinit
?gateopen @ 0 = IF
 ." The portcullis is closed, preventing entry to the keep door." CR
 ?gatekey @ 1 = IF
 ." You put the key inside a hole in the stone. "
 ." There is a mechanism which releases a counterweight, "
 ." and opens the portcullis." CR
 3 ?gatekey !  \ remove the key from play
 1 ?gateopen ! \ open the gate!
 THEN
 ELSE
 ." The portcullis is open." CR
 THEN
." 1. Go back to the entrace of the garden." CR
?gateopen @ 1 = IF
  ." 2. Enter the tower." CR THEN
ask
\ answer @
CASE
1 OF chap00 ENDOF
2 OF ?gateopen @ 1 = IF chap09 ELSE retry chap05 THEN ENDOF \ THEN
retry chap05
ENDCASE 
; is chap05


:NONAME ( chap06 )
." You dive into the pond. It is muddy. Something inside ate you."
theend
; is chap06


\ this chapter use a different structure for the 
\ test, it works better for older forth without CASE, but is 
\ more complicated to write

:NONAME ( chap07: leaving )
chapinit
." You leave the garden, approching the main gate of the domain." CR
." There is a tunnel on the right." CR
." 1. Go back to the garden again" CR
." 2. Leave once for all" CR
." 3. Enter the FORTH tunnel" CR 
ask
 answer !  \ keep this one for the test
answer @ 1 = if chap00 else
answer @ 2 = if chap08 else
answer @ 3 = if CR ." You're on your own now!" CR backtoforth else
retry chap07
\ ." error. "
then then then \ drop
; is chap07



:NONAME ( chap08: search bucket )
chapinit
." There is a dagger inside the bucket. It is emiting some light. "
." You grab it!" 
 1 ?dagger !   \ now has dagger
 CR

." 1. Continue" CR
ask
CASE
1 OF chap01 ENDOF
retry chap08
ENDCASE
; is chap08


:NONAME ( chap09: enter dungeon )
chapinit
." You stay at the dungeon's gate now. "
." It is open, but is leading to a dark corridor." CR
." 1. Go further" CR
." 2. Go back to the garden " CR
ask
CASE
1 OF chap10 ENDOF 
2 OF chap05 ENDOF 
retry chap09
ENDCASE 
; is chap09


:NONAME ( chap10 )
chapinit
." The shadow is soon covering the entrance as you walk by this place. "
." It is intimidating." CR
." 1. Turn back" CR 
?dagger @ 1 = IF 
    ." 2. With your fiercy dagger, you dare to explore more." CR THEN 
ask
CASE
1 OF chap09 ENDOF
2 OF ?dagger @ 1 = IF chap03 ELSE retry chap10 THEN ENDOF
retry chap10
ENDCASE 
; is chap10


: start
0 demo !
 cr
 chap00
;

: run 
\ init  \ start it earlier!
start 
;

run
