
# durexForth

A modern C64 Forth 


## The projects 

- [a CYOA (choose your own adventure)](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/durexforth/cyoa.fs)


![](cyoa03.png)




## More files

- [Project folder on Github](https://gitlab.com/garvalf/forth-is-fun/-/blob/main/durexforth/)


## Tips 

- In VICE C64 emulator, enable "True Drive Emulation" and elect Drive type 1541-II to make it load correctly

## Links 

- https://github.com/jkotlinski/durexforth
- [Documentation](https://jkotlinski.github.io/durexforth/)


